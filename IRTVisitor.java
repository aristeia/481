// irt visitor
package irt;

import minijava.analysis.DepthFirstAdapter;
import java.util.*;
import minijava.node.*;
import Tree.*;
import Mips.*;
import Arch.*;
import symtable.*;


public class IRTVisitor extends DepthFirstAdapter {

    private ClassTable table;
    private Node classInfo;
    private Stm result; 
    private Exp expResult;

    public Stm getIRT(){
        return this.result;
    }


    public IRTVisitor(ClassTable t){
        System.out.println("irt has been created!");
        table = t;
    }

    
    //done
    public void caseAMainClassDecl(AMainClassDecl node) {
        inAMainClassDecl(node);
        classInfo = node;
        System.out.println(((AMainClassDecl)classInfo).getId().getText() + "-- main method!");
        //table.dump();

        ClassInfo ci = table.get(((AMainClassDecl)classInfo).getId().getText() + " ");
        //System.out.println(ci.getName().getText() + " eee");
         ci.allocateMem();
        
        if(node.getId() != null)
        {
            node.getId().apply(this);
        }
        if(node.getStmt() != null)
        {
            node.getStmt().apply(this);
	    this.result = new SEQ(new LABEL(new Label("main")), new EXPR(this.expResult));
	}
        outAMainClassDecl(node);
    }

    public void caseABaseClassDecl(ABaseClassDecl node) {
        inABaseClassDecl(node);
        classInfo = node;
        ClassInfo ci = table.get(((ABaseClassDecl)classInfo).getId().getText());
        ci.allocateMem();

        if(node.getId() != null)
        {
            node.getId().apply(this);
        }
        {
            List<PVarDecl> copy = new ArrayList<PVarDecl>(node.getVarDecl());
            for(PVarDecl e : copy)
            {
                e.apply(this);
            }
        }
        {
            List<PMethod> copy = new ArrayList<PMethod>(node.getMethod());
            for(PMethod e : copy)
            {
                e.apply(this);
            }
        }
        outABaseClassDecl(node);
    }

    public void caseASubClassDecl(ASubClassDecl node)
    {
        inASubClassDecl(node);
        classInfo = node;
        ClassInfo ci = table.get(((ASubClassDecl)classInfo).getId().getText());
        ClassInfo sup = table.get(ci.getSuper().getText());
        ci.addSuper(sup);
        ci.allocateMemWithSuper();

        if(node.getId() != null)
        {
            node.getId().apply(this);
        }
        if(node.getExtends() != null)
        {
            node.getExtends().apply(this);
        }
        {
            List<PVarDecl> copy = new ArrayList<PVarDecl>(node.getVarDecl());
            for(PVarDecl e : copy)
            {
                e.apply(this);
            }
        }
        {
            List<PMethod> copy = new ArrayList<PMethod>(node.getMethod());
            for(PMethod e : copy)
            {
                e.apply(this);
            }
        }
        outASubClassDecl(node);
    }

    public void caseAMethod(AMethod node)
    {
        String nameForLater = "";
        if(classInfo instanceof AMainClassDecl){
            // do nothing
        } else if (classInfo instanceof ABaseClassDecl){
           
            ClassInfo ci = table.get(((ABaseClassDecl)classInfo).getId().getText());
            MethodTable mt = ci.getMethodTable();
            MethodInfo mi = mt.get(node.getId().getText());
            mi.allocateMem();
            nameForLater = ((ABaseClassDecl)classInfo).getId().getText();

        } else if(classInfo instanceof ASubClassDecl){

            ClassInfo ci = table.get(((ASubClassDecl)classInfo).getId().getText());
            MethodTable mt = ci.getMethodTable();
            MethodInfo mi = mt.get(node.getId().getText());
            mi.allocateMem();
            nameForLater = ((ASubClassDecl)classInfo).getId().getText();
            
        }
        if(node.getType() != null)
        {
            node.getType().apply(this);
        }
        if(node.getId() != null)
        {
            node.getId().apply(this);
        }
	{
            List<PFormal> copy = new ArrayList<PFormal>(node.getFormal());
            for(PFormal e : copy)
            {
                e.apply(this);
            }
        }
        {
            List<PVarDecl> copy = new ArrayList<PVarDecl>(node.getVarDecl());
            for(PVarDecl e : copy)
            {
                e.apply(this);
            }
        }
        {
            List<PStmt> copy = new ArrayList<PStmt>(node.getStmt());
	    copy.remove(0).apply(this);
	    SEQ statements = new SEQ(this.result, null);
	    SEQ temp = statements;
            for(PStmt e : copy)
            {
                e.apply(this);
		// temp.tail = new SEQ(this.result, null);
		// temp = temp.tail;
	    }
	    this.result = new SEQ(new LABEL(new Label(nameForLater+"."+node.getId().getText())), temp);
        }
    }

    //doneeeeeeeeee
    public void caseAMethodExp(AMethodExp node)
    {
        inAMethodExp(node);
        String nameForLater = "";
        if(classInfo instanceof AMainClassDecl){
            // do nothing
        } else if (classInfo instanceof ABaseClassDecl){
           
          
            nameForLater = ((ABaseClassDecl)classInfo).getId().getText();

        } else if(classInfo instanceof ASubClassDecl){

           
            nameForLater = ((ASubClassDecl)classInfo).getId().getText();
            
        }

	Exp obj = null;
        if(node.getObj() != null)
        {
            node.getObj().apply(this);
	    obj = expResult;
	}
        if(node.getId() != null)
        {
            node.getId().apply(this);
        }
        {
	    ExpList args = new ExpList(obj, null);
	    ExpList temp = args;
	    List<PExp> copy = new ArrayList<PExp>(node.getArgs());
	    for(PExp e : copy){
		e.apply(this);
    		temp.tail = new ExpList(this.expResult, null);
    		temp = temp.tail;
	    }
	    this.expResult = new CALL(new NAME(new Label(nameForLater+"."+node.getId().getText())), args);
        }
        outAMethodExp(node);
    }


    //done
    public void caseAPrintStmt(APrintStmt node)
    { 
        inAPrintStmt(node);
        if(node.getExp() != null){	    
	    node.getExp().apply(this);
	    this.result = new EXPR( new CALL(new NAME(new Label("print")), new ExpList(this.expResult, null)));
	}
        outAPrintStmt(node);
    }

    //done
    public void caseAThisExp(AThisExp node)
    {
        inAThisExp(node);
        
        if(classInfo instanceof AMainClassDecl){
            // do nothing
        } else if (classInfo instanceof ABaseClassDecl){
            ClassInfo ci = table.get(((ABaseClassDecl)classInfo).getId().getText() + " ");
            this.expResult = ci.getIRTinfo().getTree(new REG( new Reg( "$sp") ));
        } else if(classInfo instanceof ASubClassDecl){
            ClassInfo ci = table.get(((ASubClassDecl)classInfo).getId().getText() + " ");
            this.expResult = ci.getIRTinfo().getTree(new REG( new Reg( "$sp") ));
          
        }
        
	outAThisExp(node);
    }

    //done
    public void caseAIdExp(AIdExp node)
    {
        inAIdExp(node);
        if(node.getId() != null)
        {
           node.getId().apply(this);
           if(classInfo instanceof AMainClassDecl){
               // do nothing
           } else if (classInfo instanceof ABaseClassDecl){
               ClassInfo ci = table.get(((ABaseClassDecl)classInfo).getId().getText() + " ");
               this.expResult = ci.getVarTable().genIRT(node.getId().getText()); 
           } else if(classInfo instanceof ASubClassDecl){
               ClassInfo ci = table.get(((ASubClassDecl)classInfo).getId().getText() + " ");
              this.expResult = ci.getVarTable().genIRT(node.getId().getText()); 
             
           }
	       
           //new MEM(new BINOP(0,stackPointer,classInfo.get() ));
        }
        outAIdExp(node);
    }
    
    //done
    public void caseANumExp(ANumExp node)
    {
        inANumExp(node);
        if(node.getNum() != null)
        {
            node.getNum().apply(this);
	    this.expResult = new CONST(Integer.parseInt(node.toString()));
        }
        outANumExp(node);
    }


    //done maybe
    public void caseALengthExp(ALengthExp node)
    {
        inALengthExp(node);
        if(node.getExp() != null)
        {
           node.getExp().apply(this);
	       this.expResult =  new MEM(this.expResult);
	   }
        outALengthExp(node);
    }

    //see line 18 of simplearray
    //done
    public void caseARefExp(ARefExp node)
    {
        inARefExp(node);
        Exp name = null;
	if(node.getName() != null)
        {
            node.getName().apply(this);
	    name = this.expResult;
	}
        if(node.getIdx() != null)
        {
	    node.getIdx().apply(this);
	    this.expResult = new BINOP(0, name, this.expResult);
	}
        outARefExp(node);
    }

    //done
    public void caseATimesExp(ATimesExp node)
    {
        inATimesExp(node);
	Exp temp = null;
        if(node.getLeft() != null)
        {
            node.getLeft().apply(this);
	    temp = this.expResult;
	}
        if(node.getRight() != null)
        {
            node.getRight().apply(this);
	    this.expResult = new BINOP(2,temp,this.expResult);
	}
        outATimesExp(node);
    }

    //done
    public void caseAMinusExp(AMinusExp node)
    {
        inAMinusExp(node);
	Exp temp = null;
        if(node.getLeft() != null)
        {
            node.getLeft().apply(this);
	    temp = this.expResult;
        }
        if(node.getRight() != null)
        {
            node.getRight().apply(this);
	    this.expResult = new BINOP(1,temp,this.expResult);
        }
        outAMinusExp(node);
    }

    //done
    public void caseAPlusExp(APlusExp node)
    {
        inAPlusExp(node);
        Exp temp = null;
        if(node.getLeft() != null)
        {
            node.getLeft().apply(this);
	    temp = expResult;
        }
        if(node.getRight() != null)
        {
            node.getRight().apply(this);
	    this.expResult = new BINOP(0,temp,this.expResult);
        }
        outAPlusExp(node);
    }


    //hella work to be done
    public void caseAArrayAsmtStmt(AArrayAsmtStmt node)
    {
        inAArrayAsmtStmt(node);
        if(node.getId() != null)
        {
            node.getId().apply(this);
        }
        if(node.getIdx() != null)
        {
            node.getIdx().apply(this);
        }
        if(node.getVal() != null)
        {
            node.getVal().apply(this);
        }
        outAArrayAsmtStmt(node);
    }

    //done
    public void caseAAsmtStmt(AAsmtStmt node)
    {
        inAAsmtStmt(node);
	Exp id = null;
        if(node.getId() != null)
        {
            node.getId().apply(this);
	    id = this.expResult;
	}
        if(node.getExp() != null)
        {
            node.getExp().apply(this);
	    this.result = new MOVE(id, this.expResult);
        }
        outAAsmtStmt(node);
    }

    //done
    public void caseANewExp(ANewExp node)
    {
	inANewExp(node);
	REG gpr =  new REG(new Reg("$gp"));
        if(node.getId() != null)
        {
            node.getId().apply(this);
	    int numVarz = 0;
        if(classInfo instanceof AMainClassDecl){
            // do nothing
        } else if (classInfo instanceof ABaseClassDecl){

        if(table.get(((ABaseClassDecl)classInfo).getId().getText()).getSuper() != null) 
            numVarz += table.get(table.get(((ABaseClassDecl)classInfo).getId().getText()).getSuper().getText()).getVarTable().size();
           
            numVarz += table.get(((ABaseClassDecl)classInfo).getId().getText()).getVarTable().size();
        } else if(classInfo instanceof ASubClassDecl){
            if(table.get(((ASubClassDecl)classInfo).getId().getText()).getSuper() != null) 
              numVarz += table.get(table.get(((ASubClassDecl)classInfo).getId().getText()).getSuper().getText()).getVarTable().size();
            
            numVarz += table.get(((ASubClassDecl)classInfo).getId().getText()).getVarTable().size();
        }
	    
	    
        if (numVarz == 0)
		  this.expResult = new ESEQ(new MOVE(gpr,new CONST(0)) ,gpr);
	    else {
    		MOVE mally = new MOVE(gpr, new CALL(new NAME(new Label("malloc")), new ExpList(new CONST(numVarz*4), null)));
    		MOVE setToZero = new MOVE(new MEM(new BINOP(0, gpr,new CONST(0))), new CONST(0));
    		SEQ temp = new SEQ(mally,setToZero);
    		for(int i=1; i<numVarz; i++)
    		    temp = new SEQ(temp, new MOVE(new MEM(new BINOP(0, gpr,new CONST(i*4))), new CONST(0)));
    		
            this.expResult = new ESEQ( temp ,gpr);
        }
	}
        outANewExp(node);
    }




}



