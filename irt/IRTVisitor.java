// irt visitor
package irt;

import minijava.analysis.DepthFirstAdapter;
import java.util.*;
import minijava.node.*;
import Tree.*;
import Mips.*;
import Arch.*;
import symtable.*;
import java.lang.Math;
import types.Types;

public class IRTVisitor extends DepthFirstAdapter {

    private ClassTable table;
    private Node classInfo;
    private Stm result; 
    private Exp expResult;
    private String currentMethod;
    private ClassInfo objClass;
    private int labelNum;

    private String makeLabNum(){
	return ""+(this.labelNum++);}

    public Stm getIRT(){
        return this.result;
    }


    public IRTVisitor(ClassTable t){
       table = t;
	   labelNum=0;
    }

    
    //done
    public void caseAMainClassDecl(AMainClassDecl node) {
        inAMainClassDecl(node);
        classInfo = node;
        ClassInfo ci = table.get(((AMainClassDecl)classInfo).getId().getText() + " ");
	ci.allocateMem();
	if(node.getId() != null)
        {
            node.getId().apply(this);
        }
        if(node.getStmt() != null)
        {
            SEQ tempResult = null;
            if (this.result != null){ tempResult = new SEQ(this.result, null);} // fix
            node.getStmt().apply(this);
            this.result = new SEQ(new LABEL(new Label("main")), this.result);
            if (tempResult != null){
		tempResult.right = this.result;
		this.result = tempResult;}
	}
	//this.expResult = null;
        outAMainClassDecl(node);
    }

    public void caseABaseClassDecl(ABaseClassDecl node) {
        inABaseClassDecl(node);
        System.out.println();
        classInfo = node;
        ClassInfo ci = table.get(((ABaseClassDecl)classInfo).getId().getText());
        ci.allocateMem();
        if(node.getId() != null)
	    {
		node.getId().apply(this);
	    }
        {
            List<PVarDecl> copy = new ArrayList<PVarDecl>(node.getVarDecl());
            for(PVarDecl e : copy)
		{
		    e.apply(this);
		}
        }
        {
            List<PMethod> copy = new ArrayList<PMethod>(node.getMethod());
	    StmList endResult = null;
            if (this.result != null){ endResult = new StmList(this.result, null);}
	    copy.remove(0).apply(this);
	    StmList chainedList = new StmList(this.result, null);
	    StmList tempList = chainedList;
	    for(PMethod e : copy)
		{
		    e.apply(this);
		    tempList.tail = new StmList(this.result, null);
		    tempList= tempList.tail;
		}
	    tempList.tail = endResult;
	    if (chainedList.tail == null) {
		this.result = chainedList.head; }
	    else {
		SEQ statements = new SEQ(chainedList.head, null);
		SEQ tempSeq = statements;
		tempList = chainedList.tail;   
		while (tempList.tail != null){
		    tempSeq.right = new SEQ(tempList.head, null);
		    tempSeq = (SEQ)tempSeq.right;
		    tempList = tempList.tail;   
		}
		tempSeq.right = tempList.head;
		this.result = statements;
	    }
	}
	outABaseClassDecl(node);
    }
    
    public void caseASubClassDecl(ASubClassDecl node)
    {
        inASubClassDecl(node);
        classInfo = node;
        ClassInfo ci = table.get(((ASubClassDecl)classInfo).getId().getText());
        ClassInfo sup = table.get(ci.getSuper().getText());
        ci.addSuper(sup);
        ci.allocateMemWithSuper();

        if(node.getId() != null)
        {
            node.getId().apply(this);
        }
        if(node.getExtends() != null)
        {
            node.getExtends().apply(this);
        }
        {
            List<PVarDecl> copy = new ArrayList<PVarDecl>(node.getVarDecl());
            for(PVarDecl e : copy)
            {
                e.apply(this);
            }
        }
        {
            List<PMethod> copy = new ArrayList<PMethod>(node.getMethod());
	    StmList endResult = null;
            if (this.result != null){ endResult = new StmList(this.result, null);}
	    copy.remove(0).apply(this);
	    StmList chainedList = new StmList(this.result, null);
	    StmList tempList = chainedList;
	    for(PMethod e : copy)
		{
		    e.apply(this);
		    tempList.tail = new StmList(this.result, null);
		    tempList= tempList.tail;
		}
	    tempList.tail = endResult;
	    if (chainedList.tail == null) {
		this.result = chainedList.head; }
	    else {
		SEQ statements = new SEQ(chainedList.head, null);
		SEQ tempSeq = statements;
		tempList = chainedList.tail;   
		while (tempList.tail != null){
		    tempSeq.right = new SEQ(tempList.head, null);
		    tempSeq = (SEQ)tempSeq.right;
		    tempList = tempList.tail;   
		}
		tempSeq.right = tempList.head;
		this.result = statements;
	    }
	}
	outASubClassDecl(node);
    }

    public void caseAMethod(AMethod node)
    {
        // System.out.println(node.getId().getText() + " a method");
        currentMethod = node.getId().getText();

        String nameForLater = "";
        if(classInfo instanceof AMainClassDecl){
            // do nothing
        } else if (classInfo instanceof ABaseClassDecl){
           
            ClassInfo ci = table.get(((ABaseClassDecl)classInfo).getId().getText());
            MethodTable mt = ci.getMethodTable();
            MethodInfo mi = mt.get(node.getId().getText());
            //mi.allocateMem();
            nameForLater = ((ABaseClassDecl)classInfo).getId().getText();

        } else if(classInfo instanceof ASubClassDecl){

            ClassInfo ci = table.get(((ASubClassDecl)classInfo).getId().getText());
            MethodTable mt = ci.getMethodTable();
            MethodInfo mi = mt.get(node.getId().getText());
            //mi.allocateMem();
            nameForLater = ((ASubClassDecl)classInfo).getId().getText();
            
        }
        if(node.getType() != null)
        {
            node.getType().apply(this);
        }
        if(node.getId() != null)
        {
            node.getId().apply(this);
        }
	{
            List<PFormal> copy = new ArrayList<PFormal>(node.getFormal());
            for(PFormal e : copy)
            {
                e.apply(this);
            }
        }
        {
            List<PVarDecl> copy = new ArrayList<PVarDecl>(node.getVarDecl());
            for(PVarDecl e : copy)
            {
                e.apply(this);
            }
        }
        {
            List<PStmt> copy = new ArrayList<PStmt>(node.getStmt());
	    copy.remove(0).apply(this);
	    StmList chainedList = new StmList(this.result, null);
	    StmList tempList = chainedList;
	    for(PStmt e : copy)
		{
		    e.apply(this);

		    tempList.tail = new StmList(this.result, null);
		    tempList= tempList.tail;
		}
	    if (chainedList.tail == null) {
 		this.result = chainedList.head; 
	    }
	    else {
		SEQ statements = new SEQ(chainedList.head,chainedList.tail.head);
		
		if (chainedList.tail.tail == null)
		    this.result = statements;
		else {
		    SEQ tempSeq = new SEQ(statements, null);
		    tempList = chainedList.tail.tail;   
		    while (tempList.tail != null){
			tempSeq.right = tempList.head;
			tempSeq = new SEQ(tempSeq, null);
			tempList = tempList.tail;   
		    }
		    
		    tempSeq.right = tempList.head;
		    this.result = tempSeq;}
	    }
	    this.result = new SEQ(new LABEL(new Label(nameForLater+"."+node.getId().getText())), this.result);		
        }
    }

    //doneeeeeeeeee
    public void caseAMethodExp(AMethodExp node)
    {
        inAMethodExp(node);
	if(node.getObj() != null)    {
	    node.getObj().apply(this);
	}
	String objName = "";
	if (this.objClass.getMethodTable().contains(node.getId().getText()))
	    objName = this.objClass.getName().getText();
	else if (this.table.get(this.objClass.getSuper().getText()).getMethodTable().contains(node.getId().getText()))
	    objName = this.table.get(this.objClass.getName().getText()).getSuper().getText();
	if(node.getId() != null)
	    {
		node.getId().apply(this);
	    }
	{
	    ExpList args = new ExpList(this.expResult, null);
	    ExpList temp = args;
	    List<PExp> copy = new ArrayList<PExp>(node.getArgs());
	    for(PExp e : copy){
		e.apply(this);
		temp.tail = new ExpList(this.expResult, null);
		temp = temp.tail;
	    }
	    this.expResult = new CALL(new NAME(new Label(objName+"."+node.getId().getText())), args);
	    this.objClass = null;
        }
        outAMethodExp(node);
    }


    //done
    public void caseAPrintStmt(APrintStmt node)
    { 
        inAPrintStmt(node);
        if(node.getExp() != null){      
	    node.getExp().apply(this);
	    this.result = new EXPR( new CALL(new NAME(new Label("print")), new ExpList(this.expResult, null)));
	}
        outAPrintStmt(node);
    }

    public void caseAReturnStmt(AReturnStmt node)
    {
        inAReturnStmt(node);
        if(node.getExp() != null)
        {
            node.getExp().apply(this);
	    this.result = new RETURN(this.expResult);
	}
        outAReturnStmt(node);
    }
    
    //done
    public void caseAThisExp(AThisExp node)
    {
        inAThisExp(node);
        ClassInfo ci = null;
        if(classInfo instanceof AMainClassDecl){
            //do nothing
        } else if (classInfo instanceof ABaseClassDecl){
	    ci = table.get(((ABaseClassDecl)classInfo).getId().getText());
            this.expResult = ci.getIRTinfo().getTree(new REG( new Reg( "$sp") ));
        } else if(classInfo instanceof ASubClassDecl){
            ci = table.get(((ASubClassDecl)classInfo).getId().getText());
            this.expResult = ci.getIRTinfo().getTree(new REG( new Reg( "$sp") ));
	}
	this.objClass = ci;
	outAThisExp(node);
    }
    
    //done
    public void caseAIdExp(AIdExp node)
    {
        inAIdExp(node);
        if(node.getId() != null)
	    {
		node.getId().apply(this);
		expHelper(node.getId().getText()); 
	    }outAIdExp(node); 
    }
    
	//done
    public void caseANumExp(ANumExp node)
    {
        inANumExp(node);
        if(node.getNum() != null)
        {
            node.getNum().apply(this);
	    String num = node.toString().replaceAll("\\s","");
	    this.expResult = new CONST(Integer.parseInt(num));
        }
        outANumExp(node);
    }


    //done maybe
    public void caseALengthExp(ALengthExp node)
    {
        inALengthExp(node);
        if(node.getExp() != null)
        {
           node.getExp().apply(this);
	   //expHelper(node.getId().getText());
           this.expResult =  new MEM(this.expResult);
	   
	}
        outALengthExp(node);
    }
    
    //see line 18 of simplearray
    //done
    public void caseARefExp(ARefExp node)
    {
        inARefExp(node);
        BINOP name = new BINOP(0, null, null);
	if(node.getName() != null)
	    {
		node.getName().apply(this);
		expHelper(node.getName().toString());
		name.left = this.expResult;
	    }
        if(node.getIdx() != null)
	    {
		//System.out.println(node.parent());
		//Print.prExp(this.expResult);
		node.getIdx().apply(this);
		//Print.prExp(this.expResult);
		//System.out.println("\n\nStuff\n\n");
		//Print.prExp(this.expResult);
		//System.out.println(node.getIdx().toString());
		//expHelper(node.getIdx().toString());
		//System.out.println((((CONST)(((BINOP)this.expResult).right)).value+"\n\n\n\n"));
		//Print.prExp(this.expResult);
		name.right = new BINOP(2, new BINOP(0, this.expResult, new CONST(1)), new CONST(4));
		this.expResult = new MEM(name);
	    }
        outARefExp(node);
    }

    //done
    public void caseATimesExp(ATimesExp node)
    {
        inATimesExp(node);
    Exp temp = null;
        if(node.getLeft() != null)
        {
            node.getLeft().apply(this);
        temp = this.expResult;
    }
        if(node.getRight() != null)
        {
            node.getRight().apply(this);
        this.expResult = new BINOP(2,temp,this.expResult);
    }
        outATimesExp(node);
    }

    //done
    public void caseAMinusExp(AMinusExp node)
    {
        inAMinusExp(node);
    Exp temp = null;
        if(node.getLeft() != null)
        {
            node.getLeft().apply(this);
        temp = this.expResult;
        }
        if(node.getRight() != null)
        {
            node.getRight().apply(this);
        this.expResult = new BINOP(1,temp,this.expResult);
        }
        outAMinusExp(node);
    }

    //done
    public void caseAPlusExp(APlusExp node)
    {
        inAPlusExp(node);
        Exp temp = null;
        if(node.getLeft() != null)
        {
            node.getLeft().apply(this);
        temp = expResult;
        }
        if(node.getRight() != null)
        {
            node.getRight().apply(this);
        this.expResult = new BINOP(0,temp,this.expResult);
        }
        outAPlusExp(node);
    }

    public void caseAFalseExp(AFalseExp node)
    {
        inAFalseExp(node);
	    this.expResult = new CONST(0);
        outAFalseExp(node);
    }

    public void caseATrueExp(ATrueExp node)
    {
        inATrueExp(node);
        this.expResult = new CONST(1);
        outATrueExp(node);
    }
    public void caseANotExp(ANotExp node)
    {
        inANotExp(node);
        if(node.getExp() != null)
        {
            node.getExp().apply(this);
	    this.expResult =new BINOP(4,new BINOP(0, this.expResult, new CONST(1)), new CONST(1));
        }
        outANotExp(node);
    }
    

    //hella work to be done
    public void caseAArrayAsmtStmt(AArrayAsmtStmt node)
    {
        inAArrayAsmtStmt(node);
        BINOP name = new BINOP(0, null, null);
        MOVE id = new MOVE(null, null);
	if(node.getId() != null)
	    {
		node.getId().apply(this);
		expHelper(node.getId().toString());
		name.left = this.expResult;
	    }
        if(node.getIdx() != null)
	    {
		//System.out.println(node.parent());
		//Print.prExp(this.expResult);
		node.getIdx().apply(this);
		//Print.prExp(this.expResult);
		//System.out.println("\n\nStuff\n\n");
		//Print.prExp(this.expResult);
		//System.out.println(node.getIdx().toString());
		//expHelper(node.getIdx().toString());
		//System.out.println((((CONST)(((BINOP)this.expResult).right)).value+"\n\n\n\n"));
		//Print.prExp(this.expResult);
		name.right = new BINOP(2, new BINOP(0, this.expResult, new CONST(1)), new CONST(4));
		id.dst = new MEM(name);
	    }

        
        if(node.getVal() != null)
        {
            node.getVal().apply(this);
	    id.src = this.expResult;
	    this.result =id;
	
	}
        outAArrayAsmtStmt(node);
    }

    //done
    public void caseAAllocExp(AAllocExp node)
    {
        inAAllocExp(node);
        if(node.getExp() != null)
        {
            node.getExp().apply(this);
	    REG s0 =  new REG(new Reg("$8"));
	    Exp len = this.expResult;
	    MOVE alloc = new MOVE(s0, new CALL(new NAME(new Label("malloc")), new ExpList(new BINOP(2, new BINOP(0,len, new CONST(1)), new CONST(4)), null)));
	    int lenint = 0;
	    Stm temp = new MOVE(new MEM(s0), len);
	    /*MEM pointer = new MEM(s0);
	    if (len instanceof CONST) {
		lenint = ((CONST)len).value;
		for(int i=1; i<lenint+1; i++){
		    temp = new SEQ(temp, new MOVE(new MEM(new BINOP(0, pointer.exp, new BINOP(2, new CONST(i), new CONST(4)))), new CONST(0)));
		    }}*/
	    SEQ allocAndLen = new SEQ( alloc, temp);
	    this.expResult = new ESEQ( allocAndLen, s0);
        }
        outAAllocExp(node);
    }

    //done
    public void caseAAsmtStmt(AAsmtStmt node)
    {
        inAAsmtStmt(node);
	MOVE id = new MOVE(null, null);
	if(node.getId() != null)
	    {
		node.getId().apply(this);
		expHelper(node.getId().getText());
		id.dst = this.expResult;
	    }
        if(node.getExp() != null)
	    {
		node.getExp().apply(this);
		id.src = this.expResult;
		this.result =id;
	    }
        outAAsmtStmt(node);
    }


   public void caseAWhileStmt(AWhileStmt node)
   {
      inAWhileStmt(node);
      Label loop = new Label(this.makeLabNum());
      Label t = new Label(this.makeLabNum());
      Label end = new Label(this.makeLabNum());
      CJUMP whilecond = new CJUMP(0, null, new CONST(0),end, t );
      if(node.getExp() != null)
      {
         node.getExp().apply(this);
	 whilecond.left = this.expResult;
      }
      SEQ condition = new SEQ(new LABEL(loop), whilecond);
      SEQ iftrue = new SEQ(new LABEL(t), null);
      if(node.getStmt() != null)
      {
         node.getStmt().apply(this);
	 iftrue.right = new SEQ(this.result, new JUMP(loop));
      }      
      this.result = new SEQ(condition, new SEQ(iftrue, new LABEL(end)));
      //Print.prStm(this.result);
      //System.out.println("\n\n\n\n\n");
      outAWhileStmt(node);
   }
   

    
    public void caseAIfStmt(AIfStmt node)
    {
        inAIfStmt(node);
	Label t = new Label(this.makeLabNum());
	Label f = new Label(this.makeLabNum());
	Label end = new Label(this.makeLabNum());
	CJUMP condition = new CJUMP(0, null, new CONST(1),t, f );
	if(node.getExp() != null)
        {
            node.getExp().apply(this);
	    condition.left = this.expResult;
        }
	SEQ iftrue = new SEQ(new LABEL(t), null);
	SEQ iffalse = new SEQ(new LABEL(f), null);
        if(node.getYes() != null)
        {
            node.getYes().apply(this);
	    iftrue.right = new SEQ(this.result, new JUMP(end));
        }
        if(node.getNo() != null)
        {
            node.getNo().apply(this);
	    iffalse.right = new SEQ(this.result, new JUMP(end));
        }
	this.result = new SEQ(condition, new SEQ(iffalse, new SEQ(iftrue, new LABEL(end))));
        outAIfStmt(node);
    }


    
    public void caseABlockStmt(ABlockStmt node)
    {
        inABlockStmt(node);
        {
            List<PStmt> copy = new ArrayList<PStmt>(node.getStmt());
	    copy.remove(0).apply(this);
            Stm temp = this.result;
	    for(PStmt e : copy)
            {
		temp = new SEQ(temp, null);
                e.apply(this);
		((SEQ)temp).right = this.result;
            }
	    this.result = temp;
        }
        outABlockStmt(node);
    }


    public void caseAAndExp(AAndExp node)
   {
       inAAndExp(node);
       
       REG ourReg = new REG(new Reg("8"));
       Exp temp = null;
       if(node.getLeft() != null)
	   {
	       node.getLeft().apply(this);
	       temp = this.expResult;
	   }
       if(node.getRight() != null)
	   {
	       node.getRight().apply(this);
	   }
       BINOP tempadd = new BINOP(0, temp, this.expResult);
       Label lefty = new Label(this.makeLabNum());
       Label righty = new Label(this.makeLabNum());
       Label home = new Label(this.makeLabNum());
       
       SEQ leftSide = new SEQ(new LABEL(lefty), new SEQ(new MOVE(ourReg, new CONST(1)), new JUMP(home)));
       SEQ rightSide = new SEQ(new LABEL(righty), new SEQ(new MOVE(ourReg, new CONST(0)), new JUMP(home)));
       SEQ bigRight = new SEQ(rightSide, new LABEL(home));
       
       
       CJUMP and = new CJUMP(0, tempadd, new CONST(2), lefty, righty);
       
       SEQ t = new SEQ(new COMMENT("&&"), new SEQ(and, new SEQ(leftSide, bigRight)));
       // change reg//
       ESEQ topLevel = new ESEQ(t, ourReg);
       this.expResult = topLevel;
       outAAndExp(node);
   }
    
   
   public void caseALtExp(ALtExp node)
   {
       inALtExp(node);

       REG ourReg = new REG(new Reg("8"));
       Exp temp = null;
      if(node.getLeft() != null)
      {
         node.getLeft().apply(this);
         temp = this.expResult;
      }
      if(node.getRight() != null)
      {
         node.getRight().apply(this);
      }

      Label lefty = new Label(this.makeLabNum());
      Label righty = new Label(this.makeLabNum());
      Label home = new Label(this.makeLabNum());

      SEQ leftSide = new SEQ(new LABEL(lefty), new SEQ(new MOVE(ourReg, new CONST(1)), new JUMP(home)));
      SEQ rightSide = new SEQ(new LABEL(righty), new SEQ(new MOVE(ourReg, new CONST(0)), new JUMP(home)));
      SEQ bigRight = new SEQ(rightSide, new LABEL(home));


      CJUMP lt = new CJUMP(2, temp, this.expResult, lefty, righty);

      SEQ t = new SEQ(new COMMENT("<"), new SEQ(lt, new SEQ(leftSide, bigRight)));
        // change reg//
        ESEQ topLevel = new ESEQ(t, ourReg);
        this.expResult = topLevel;
        outALtExp(node);
   }
   

    //done
    public void caseANewExp(ANewExp node) {
	inANewExp(node);
	REG gpr =  new REG(new Reg("$gp"));
	if(node.getId() != null)
	    {
		node.getId().apply(this);
		this.objClass = this.table.get(node.getId().getText());
		if (this.objClass.getSuper() == null)
		    this.objClass.allocateMem();
		else
		    this.objClass.allocateMemWithSuper();
		this.expResult = this.objClass.genIRT();
		/* //int numVarz = 0;	
		if (objClass.getSuper() == null ){
		    if(table.get(((ABaseClassDecl)classInfo).getId().getText()).getSuper() != null) 
			numVarz += table.get(table.get(((ABaseClassDecl)classInfo).getId().getText()).getSuper().getText()).getVarTable().size();
		    
		    numVarz += table.get(((ABaseClassDecl)classInfo).getId().getText()).getVarTable().size();
		} else {
		    if(table.get(((ASubClassDecl)classInfo).getId().getText()).getSuper() != null) 
			numVarz += table.get(table.get(((ASubClassDecl)classInfo).getId().getText()).getSuper().getText()).getVarTable().size();
		    
		    numVarz += table.get(((ASubClassDecl)classInfo).getId().getText()).getVarTable().size();
		}
		
		
		if (numVarz == 0)
		    this.expResult = new ESEQ(new MOVE(gpr,new CONST(0)) ,gpr);
		else {
		    MOVE mally = new MOVE(gpr, new CALL(new NAME(new Label("malloc")), new ExpList(new CONST(numVarz*4), null)));
		    MOVE setToZero = new MOVE(new MEM(new BINOP(0, gpr,new CONST(0))), new CONST(0));
		    SEQ temp = new SEQ(mally,setToZero);
		    for(int i=1; i<numVarz; i++)
			temp = new SEQ(temp, new MOVE(new MEM(new BINOP(0, gpr,new CONST(i*4))), new CONST(0)));
		    
		    this.expResult = new ESEQ( temp ,gpr);
		}*/
	    }
        outANewExp(node);
    }
    
    
    private void expHelper(String node){
	node = node.toString().replaceAll("\\s","");
	REG gp =  new REG(new Reg("$gp"));
	REG sp =  new REG(new Reg("$sp"));
	REG rightReg=null;
	ClassInfo ci = null;
	if(classInfo instanceof AMainClassDecl){
	    // do nothing
	} else if (classInfo instanceof ABaseClassDecl){
	    ci = table.get(((ABaseClassDecl)classInfo).getId().getText());}
	else if (classInfo instanceof ASubClassDecl){
	    ci = table.get(((ASubClassDecl)classInfo).getId().getText());}
	
	VarTable tempTable = null;
	boolean instance = true;
	int numVars = 0;
	if(ci.getMethodTable().get(currentMethod).getLocals().get(node) != null){
	    tempTable = ci.getMethodTable().get(currentMethod).getLocals();
	    rightReg = sp;
	    instance = false;}
	else if(ci.getVarTable().get(node) != null){
	    rightReg = sp;
	    tempTable = ci.getVarTable();    
	    if(classInfo instanceof ASubClassDecl)
		numVars= ci.getVarTable().size()*4;}
	else if(classInfo instanceof ASubClassDecl){
	    rightReg = sp;
	    ClassInfo superi = table.get( ci.getSuper().getText());
	    if(superi.getVarTable().get(node) != null){
		tempTable = superi.getVarTable(); 
	    }}    
	else {
	    System.out.println("var doesnt exit"+node+" "+ci.getName());}
	
	if (tempTable != null)  {
	    this.expResult = tempTable.getInfo(node).getAccess().getTree(rightReg);
	    if (instance){
		this.expResult = new MEM(new BINOP(0, this.expResult, new CONST(numVars)));}
	    PType tempType = tempTable.get(node);
	    if (tempType  instanceof AUserType) {
		this.objClass = this.table.get(Types.toStr(tempType)); } 
	}
    }
}



