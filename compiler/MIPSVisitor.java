package compiler;

import java.util.*;
import minijava.node.*;
import Tree.*;
import Mips.*;
import Arch.*;
import symtable.*;
import java.lang.*;
import types.Types;


class RegVal {
    public int start;
    public int end;
    
    public RegVal(){}
}

class MyRegister extends Object {
    
    public String name;
    public Exp val;
    public int changed;
    
    public MyRegister(String n, Exp v){
	this.name=n;
	this.val=v;
    }
    public MyRegister(String n, Exp v, int c){
	this.name=n;
	this.val=v;
	this.changed = c;
    }
    
    public boolean equals(MyRegister other) {
	if(PrintToString.prExp(this.val).equals(PrintToString.prExp(other.val)))
	    return true;
	return false;
    }
}

public class MIPSVisitor {
    
    private ArrayDeque<MyRegister> sRegisters;
    //private MyRegister[] vRegisters;
    //private MyRegister[] aRegisters;
    //private Stm IRT;
    
    private HashMap<String, MyRegister> tempRegs;
    private HashMap<LABEL, ArrayList<String>> methods;
    private LABEL currentMethod;
    private LABEL oldMethod;
    private HashMap<String, Integer> args;
    //private NAME currentCall;

    private MyRegister usedReg;

    public MIPSVisitor(Stm input){
	this.sRegisters = new ArrayDeque<MyRegister>(8); //max number of temp registers
	//this.aRegisters = new MyRegister[4];
	//this.vRegisters = new MyRegister[2];
	this.args = new HashMap<String, Integer>();
	this.methods =new HashMap<LABEL, ArrayList<String>>();
	this.tempRegs = new HashMap<String,MyRegister>();
	//Print.prStm(input);
	this.resolver(input);
    }

    private void resolve(BINOP input) {
	this.resolver(input.left);
	MyRegister left = this.usedReg;
	String stm = left.name.toString();
	String res;
	if(stm.length() == 2)
	    res = stm;
	else{
	    res = "$"+available();
	    removeReg(Integer.parseInt(res.substring(1)));}
	//Print.prExp(input.right);
	if((input.right instanceof CONST) && (input.binop != 2)){
	    stm = "i "+ res +" "+ stm + " "+ ((CONST)input.right).value;}
	else {
	    this.resolver(input.right);
	    stm = " "+ res +" "+stm+" "+usedReg.name.toString();}
	switch(input.binop) {
	case 0: stm = "add"+stm; break;
	case 1: stm = "sub"+stm; break;
	case 2: stm = "mul"+stm; break;}
	this.methods.get(this.currentMethod).add(stm);
       	this.usedReg = new MyRegister(res, input, this.methods.get(this.currentMethod).size()-1);
	this.sRegisters.addFirst(this.usedReg);
    }

    private void resolve(CALL input) {
	ExpList temp = input.kids();
	int a=0;
	boolean gp = true;
	ArrayList<String> givenMethods = new ArrayList<String>();
	givenMethods.add("malloc");
	givenMethods.add("print");
	givenMethods.add("exit");
	ArrayList<MyRegister> args = new ArrayList<MyRegister>();
	do {
	    temp = temp.tail;
	    this.resolver(temp.head);
		/*System.out.print("addi $a"+(a++)+" "+temp.head.toString());
		  System.out.println(this.usedReg.name.toString()+" 0");*/
	    if (gp && (! givenMethods.contains(((NAME)input.func).label.toString()))){
		gp = false;
		this.methods.get(this.currentMethod).add("addi $gp "+this.usedReg.name.toString()+" 0");}
	    else{
		args.add(this.usedReg);
		this.methods.get(this.currentMethod).add("addi $a"+(a++)+" "+this.usedReg.name.toString()+" 0");
	    }
	    
	}
	while(temp.tail != null) ;
	this.args.put(((NAME)input.func).label.toString(), new Integer(a-1));
	this.methods.get(this.currentMethod).add("jal "+((NAME)input.func).label.toString());
	this.usedReg = new MyRegister("$v0", input, this.methods.get(this.currentMethod).size()-1);
	//System.out.println("jump "+input.func.toString());
    }
    
    private void resolve(CJUMP input) {
	this.resolver(input.left);
	MyRegister l = this.usedReg;
	this.resolver(input.right);
	MyRegister r = this.usedReg;
	if(input.relop==0){
	    this.methods.get(this.currentMethod).add("beq "+l.name+" "+r.name+" L"+input.iftrue.toString());
	    this.methods.get(this.currentMethod).add("j L"+input.iffalse.toString());}
	else {
	    String name = "$" + available();
	    removeReg(Integer.parseInt(name.substring(1)));
	    this.methods.get(this.currentMethod).add("slt "+name+" "+l.name+" "+r.name);
	    this.methods.get(this.currentMethod).add("bne "+name+" $zero L"+input.iftrue.toString());
	    this.methods.get(this.currentMethod).add("j L"+input.iffalse.toString());
	    this.usedReg = new MyRegister(name, input.left, this.methods.get(this.currentMethod).size()-1);
	    this.sRegisters.addFirst(this.usedReg);	}   
	if (this.currentMethod.label.toString().charAt(0)=='L'){
	    this.currentMethod = this.oldMethod;
	    this.oldMethod=null;}
    }

    private void resolve(COMMENT input){ 
	this.methods.get(this.currentMethod).add("# "+input.text);	
    }

    private void resolve(CONST input) {
	String name = "$" + available();
	removeReg(Integer.parseInt(name.substring(1)));
	this.methods.get(this.currentMethod).add("li "+name+" " +input.value);
	//System.out.println("li "+name+" " +input.value);
       	this.usedReg = new MyRegister(name, input, this.methods.get(this.currentMethod).size()-1);
	this.sRegisters.addFirst(this.usedReg);	
    }

    private void resolve(EXPR input) {
	this.resolver(input.exp);
    }

    private void resolve(ESEQ input) {
	this.resolver(input.stm);
	this.resolver(input.exp);
    }
    
    private void resolve(JUMP input) {
	this.methods.get(this.currentMethod).add("j L"+input.targets.head.toString());
	this.currentMethod = this.oldMethod;
	this.oldMethod=null;
    }

    private void resolve(LABEL input) {
	if (input.label.toString().matches("\\d+"))
	    input.label = new Label("L"+input.label.toString());
	if(this.currentMethod != null)
	    this.oldMethod = this.currentMethod;
	this.currentMethod = input;
	this.methods.put(input, new ArrayList<String>());
	if ( input.label.toString().equals("main"))
	    this.methods.get(input).add("sw  $ra, 0($sp)");
	//System.out.println("Added method "+input.label.toString());
    }

    private void resolve(MEM input) {
	this.resolver(input.exp);
	String name = "$" + available();
	removeReg(Integer.parseInt(name.substring(1)));
	//Print.prExp(input.exp);
	//System.out.println(" "+this.usedReg.name.toString() );
	this.methods.get(this.currentMethod).add("lw "+name+" 0("+this.usedReg.name.toString()+")");
	this.usedReg = new MyRegister(name, input, this.methods.get(this.currentMethod).size()-1);
	this.sRegisters.addFirst(this.usedReg);	
    }
    
    private void resolve(MOVE input) {
	//Print.prStm(input);
	if(input.dst instanceof REG){
	    this.resolver(input.dst);
	    MyRegister dst = this.usedReg;
	    if(input.src instanceof CONST)
		this.methods.get(this.currentMethod).add("li " + dst.name+ " " + ((CONST)input.src).value);
	    else{
		this.resolver(input.src);
		this.methods.get(this.currentMethod).add("addi " + dst.name + " " + this.usedReg.name +" 0");}}
	else{
	    this.resolver(input.src);
	    MyRegister temp = this.usedReg;
	    MEM tempExp = null;
	    if (input.dst instanceof ESEQ && ((ESEQ)input.dst).exp instanceof MEM)
		tempExp =(MEM) ((ESEQ)input.dst).exp ;
	    else if(input.dst instanceof MEM)
		tempExp =(MEM) input.dst;
	    
	    if( tempExp != null ) {
		if (tempExp.exp instanceof BINOP || tempExp.exp instanceof CONST){
		    if(((BINOP)tempExp.exp).right instanceof CONST && ((BINOP)tempExp.exp).binop<2 ){
			this.resolver(((BINOP)tempExp.exp).left);
			this.methods.get(this.currentMethod).add("sw " + temp.name + " "+(((CONST)((BINOP)tempExp.exp).right).value * (((BINOP)tempExp.exp).binop*(-2)+1))+"("+this.usedReg.name+")");}
		    else { 
			this.resolver(tempExp.exp);
			this.methods.get(this.currentMethod).add("sw " + temp.name + " 0("+this.usedReg.name+")");}}
		else { 
		    this.resolver(tempExp.exp);
		    this.methods.get(this.currentMethod).add("sw " + temp.name + " 0("+this.usedReg.name+")");}}
	    else {
		this.resolver(input.dst);
		this.methods.get(this.currentMethod).add("addi " + this.usedReg.name + " " + temp.name+" 0");}}}
     
    /*private void resolve(NAME input) {
	this.currentCall = input;
	}*/

    private void resolve(REG input) {
	if (input.reg.toString().charAt(0)!='$'){
	    if (this.tempRegs.keySet().contains(input.reg.toString()))
		this.usedReg = this.tempRegs.get(input.reg.toString());
	    else{
		String name = "$" + available();
		removeReg(Integer.parseInt(name.substring(1)));
		this.usedReg = new MyRegister(name, input, this.methods.get(this.currentMethod).size()-1);
		this.sRegisters.addFirst(this.usedReg); 
		this.tempRegs.put(input.reg.toString(), this.usedReg);}}
	else
	    this.usedReg = new MyRegister(input.reg.toString(), input, this.methods.get(this.currentMethod).size()-1);
    }

    private void resolve(RETURN input) {
	this.resolver(input.ret);
	this.methods.get(this.currentMethod).add("addi $v0 "+this.usedReg.name.toString()+" 0");
	this.usedReg = new MyRegister("$v0", this.usedReg.val, this.methods.get(this.currentMethod).size()-1);
	/*if (this.oldMethod == null)
	    this.methods.get(this.currentMethod).add(this.currentMethod.label.toString());
	else{
	    LABEL[] keys = new LABEL[this.methods.size()];
	    this.methods.keySet().toArray(keys);
	    for (LABEL key2: keys){
		if (key2.label.toString().equals (this.oldMethod.label.toString()))
		    this.methods.get(key2).add(this.currentMethod.label.toString());}}*/
	this.oldMethod = null;
	this.currentMethod = null;
    }
    
    
    private void resolve(SEQ input) {
	this.resolver(input.left);
	this.resolver(input.right);	
    }

    private int available() {
	//System.out.println("First");
	int[] available = {8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25};
	MyRegister[] sRegs = new MyRegister[sRegisters.size()];
	this.sRegisters.toArray(sRegs);
	for (MyRegister temp:sRegs) {
	    //System.out.println(temp.name);
	    available[Integer.parseInt(temp.name.substring(1))-8] = -1;
	}
	for (int i=0; i<available.length; i++)
	    if(available[i] != -1)
		return available[i];
	return Integer.parseInt(this.sRegisters.peekLast().name.substring(1));}
    
    private void removeReg(int n){
	MyRegister[] list = new MyRegister[this.sRegisters.size()];
	this.sRegisters.toArray(list);
	for(MyRegister m  :list)
	    if(Integer.parseInt(m.name.substring(1)) == n)
		this.sRegisters.remove(m);}
    
    public void resolver(Exp e) {
	/*if (sRegisters.contains(new MyRegister("temp", e))){
	    MyRegister tempReg = new MyRegister("temp", e);
	    MyRegister[] sregarr = new MyRegister[sRegisters.size()];
	    this.sRegisters.toArray(sregarr);
	    for(MyRegister tempReg2: sregarr){
		if (tempReg.equals(tempReg2))
		    if(notChanged(tempReg2))
			usedReg = tempReg2;}}
			else{*/
	    //Print.prExp(e);
	    if (e instanceof BINOP) resolve((BINOP)e);
	    else if (e instanceof MEM) resolve((MEM)e);
	    else if (e instanceof REG) resolve((REG)e);
	    else if (e instanceof ESEQ) resolve((ESEQ)e);
	//else if (e instanceof NAME) resolve((NAME)e);
	    else if (e instanceof CONST) resolve((CONST)e);
	    else if (e instanceof CALL) resolve((CALL)e);
}

   public void resolver(Stm s) {
      if (s instanceof SEQ) this.resolve((SEQ)s);
      else if (s instanceof LABEL) this.resolve((LABEL)s);
      else if (s instanceof COMMENT) this.resolve((COMMENT)s);
      else if (s instanceof JUMP) this.resolve((JUMP)s);
      else if (s instanceof CJUMP) this.resolve((CJUMP)s);
      else if (s instanceof MOVE) this.resolve((MOVE)s);
      else if (s instanceof EXPR) this.resolve((EXPR)s);
      else if (s instanceof RETURN) this.resolve((RETURN)s);
   }
    
    private boolean notChanged(MyRegister r) {
	Exp e = r.val;
	String[] regs = PrintToString.prExp(e).split("\\$");
	for(int i=1;i< regs.length; i++){
	    String s = regs[i];
	    //System.out.println(s);
	    if (s.length()>2 && ! "(),. ".contains(Character.toString(s.charAt(2))) )
		s=s.substring(0,3);
	    else
		s=s.substring(0,2);
	    for(int j=r.changed+1; j<this.methods.get(this.currentMethod).size();j++)
		if(this.methods.get(this.currentMethod).get(j).split("\\s")[1].contains(s))
		    return false;
	    //System.out.println("reg= "+s+s.length());
	}return true;}
    
    private void optimizeRegUse(LABEL method){
	HashMap<String, RegVal> regs = new HashMap<String, RegVal>();
	ArrayList<String> lines = this.methods.get(method);
	for(int i=0; i<lines.size(); i++){
	    String[] linesplit = lines.get(i).split("[\\(\\),\\s]");
	    for(String word: linesplit){
		if (word.charAt(0)=='$' && (! word.substring(1,2).matches("[a-z]")))
		    if (! regs.keySet().contains(word) ){
			regs.put(word, new RegVal());
			regs.get(word).start = i;}
		    else if (! word.equals(linesplit[1]) || linesplit[0].equals("sw"))
			regs.get(word).end = i;}}
	ArrayList<String> keyset = new ArrayList<String>();
	for(String key:regs.keySet())
	    keyset.add(key);
	Collections.sort(keyset, new Comparator<String>() {
		@Override
		    public int compare(String s1, String s2) {		    
		    return  Integer.parseInt(s1.substring(1)) - (Integer.parseInt(s2.substring(1)));
		}
	    });
	for(int i=(keyset.size()-1); i>=0; i--) {
	    String key = keyset.get(i);
	    for(int j = i-1; j>=0; j--)
		if (regs.get(keyset.get(j)).end <= regs.get(key).start) {
		    //replace all instances of key with keyset.get(j)
		    for(int k=0; k<this.methods.get(method).size(); k++)
			this.methods.get(method).set(k,this.methods.get(method).get(k).replace(key, keyset.get(j)));
		    regs.get(keyset.get(j)).end = regs.get(key).end;
		    regs.remove(key);
		    break;}}
    }
    
    private LABEL addRegisterRestore(ArrayList<String> regs, ArrayList<String> lines, ArrayList<LABEL> labels, LABEL lastLabel){
	LABEL tempLabel = null;
	LABEL tempLabel2 = null;
	for(int i=0; i<lines.size(); i++){
	    String x = lines.get(i);
	    if (labels.size()>0){
		if ((i== lines.size()-1) && x.contains("$v0") && lines.equals(this.methods.get(labels.get(labels.size()-1))))
		    return labels.get(labels.size()-1);}
	    for(int y=0; y<x.length(); y++) 
		if (x.charAt(y)=='$' && Character.isDigit(x.charAt(y+1))){
		    String tempReg;
		    if (y+2<x.length() && ! "(),. ".contains(Character.toString(x.charAt(y+2))) )
			tempReg=x.substring(y,y+3);
		    else
			tempReg=x.substring(y,y+2);
		    if (! regs.contains(tempReg)) {
			regs.add(tempReg);}}
		else if (x.charAt(y)=='L' && (y+1)<x.length()){ 
		    LABEL[] keys = new LABEL[this.methods.size()];
		    this.methods.keySet().toArray(keys);
		    for (LABEL key: keys)
			if (key.label.toString().equals(x.substring(y,y+2)))
			    if (! labels.contains(key)){			   
				labels.add(key);
				tempLabel2 = addRegisterRestore(regs, this.methods.get(key),labels, lastLabel);
				if (tempLabel2 != null)
				    tempLabel = tempLabel2;}}}
	return tempLabel;}
    
    public void dump() {
	LABEL[] keys = new LABEL[this.methods.size()];
	this.methods.keySet().toArray(keys);
	//Generate Prologue and Epilogue for each method before printing the method
	for (LABEL key: keys){
	    if (! key.label.toString().equals("main") && key.label.toString().charAt(0) != 'L' ) {
		boolean labelsMentioned = false;
		for (String s: this.methods.get(key))
		    if (s.contains("L"))
			     labelsMentioned = true;
		if (! labelsMentioned)
		    this.optimizeRegUse(key);
		ArrayList<String> regs = new ArrayList<String>();
		regs.add("$ra");
		regs.add("$gp");
		for (int y=0; y<((int)this.args.get(key.label.toString()))+1;y++)
		    regs.add("$a"+y);
		ArrayList<String> tempLines = new ArrayList<String>();
		ArrayList<LABEL> labels =  new ArrayList<LABEL>();
		for (String s: this.methods.get(key))
		    tempLines.add(s);
		LABEL keyo = addRegisterRestore(regs, tempLines, labels, null);	
		if (keyo==null)
		    keyo = key;
		this.methods.get(key).add(0, "subi $sp $sp "+(4*(1+regs.size())));
		//this.methods.get(key).remove(this.methods.get(key).size()-1);
		for (int y=1; y<regs.size()+1; y++) {
		    this.methods.get(key).add(y, "sw "+regs.get(y-1)+" "+(4*(y-1))+"($sp)");
		    if( Character.isDigit( regs.get(y-1).charAt(1)))
			    this.methods.get(keyo).add("lw "+regs.get(y-1)+" "+(4*(y-1))+"($sp)");
		}
		this.methods.get(keyo).add("lw $ra 0($sp)");
		this.methods.get(keyo).add("addi $sp $sp "+(4*(1+regs.size())));
		this.methods.get(keyo).add("jr $ra");}


	    System.out.println(key.label.toString()+":");
	    for(int i=0; i< this.methods.get(key).size(); i++){
		boolean done = false;
		String line = this.methods.get(key).get(i);
		// Optimize to replace all instances of addi _ _ 0
		if( (i+1) < this.methods.get(key).size()  ){
		    String[] thisLine = line.split("\\s");
		    String tempReg = thisLine[1];
		    if (tempReg.contains("$")){
			String[] nextLine = this.methods.get(key).get(i+1).split("\\s");
			if (this.methods.get(key).get(i+1).contains(tempReg) && nextLine[nextLine.length-1].equals("0") && ! nextLine[0].equals("mul")){
			    System.out.print("    "+thisLine[0] + " "+nextLine[1]);
			    for(int j=2; j<thisLine.length; j++)
				//	if(thisLine[j].charAt(2)!='$')
				System.out.print(" "+thisLine[j]);
			    //else
		    //	    System.out.print(" "+nextLine[j]);
			    done = true;
			    System.out.println();
			    i++;}}}
		if(! done)
		    System.out.println("    "+line);}
	    if ( key.label.toString().equals("main"))
		System.out.println("    lw  $ra, 0($sp)\n    jr $ra");
	    System.out.println();}}
}
