#
# MIPS assembly header for MiniJava code.  Defines the "built-in"
# functions, and does initialization for the memory allocator.  It
# then jumps to a procedure named "main" (so there had better be one),
# and exits once main returns.
#
        .data
        .align 2
_mem:   .space 102400       # 100K of heap space
_next:  .word 0
_nl:    .asciiz "\n"
_exit:  .asciiz "Exited with value "

        .text

        la   $s0, _mem 
        la   $s1, _next  
        sw   $s0, 0($s1)    # Set up ptr to start of malloc pool
        jal  main           # Jump to start of Minijava program
        nop
        li    $v0 10
        syscall             # syscall 10 (exit)  
        
#
# Implements the "built-in" print.  It expects a single integer
# argument to be passed in $a0.
#
print:  
        li   $v0, 1         # Specify the "print int" syscall
        syscall             # Arg is in $a0, so just do call
        la   $a0, _nl       # Load addr of \n string
        li   $v0, 4         # Specify the "print string" syscall
        syscall             # Print the string
        jr   $ra        
        
#
# Implements the "built-in" exit.  It expects a single integer
# argument to be passed in $a0.
#
exit:  
        move $s0, $a0       # Store the integer arg 
        la   $a0, _exit     # Load addr of "exit" string
        li   $v0, 4         # Specify the "print string" syscall
        syscall             # Print the string
        move $a0, $s0       # Set up the integer arg for printing
        li   $v0, 1         # Specify the "print int" syscall
        syscall             # Print the integer
        la   $a0, _nl       # Load addr of \n string
        li   $v0, 4         # Specify the "print string" syscall
        syscall             # Print the \n
        li    $v0 10        # Specify the MIPS exit syscall
        syscall             # exit

#
# Implements a quick and dirty "malloc" that draws from a fixed-size
# pool of memory, and never frees or reallocates memory.  Expects a
# single integer argument to be passed in $a0.  Written so that it
# uses only $a and $v registers and therefore needs no stack frame.
# (Look into into sbrk as a better way to allocate memory.)
#
malloc: 
        addi $a0, $a0, 3    # Round up to next word boundary
        srl  $a0, $a0, 2    # Remove lowest two bits by shifting
        sll  $a0, $a0, 2    #  right and then back to left
        la   $a1, _next     # Global pointing to free memory
        lw   $v0, 0($a1)    # Load its contents
        add  $v1, $v0, $a0  # Bump up to account for this chunk
        sw   $v1, 0($a1)    # Store new value back in global
        jr   $ra


main:
    sw  $ra, 0($sp)
    li $gp 4
    jal Foo.run
    addi $a0 $v0 0
    jal print
    lw  $ra, 0($sp)
    jr $ra

Foo.run:
    subi $sp $sp 52
    sw $ra 0($sp)
    sw $gp 4($sp)
    sw $8 8($sp)
    sw $9 12($sp)
    sw $10 16($sp)
    sw $11 20($sp)
    sw $12 24($sp)
    sw $13 28($sp)
    sw $14 32($sp)
    sw $15 36($sp)
    sw $16 40($sp)
    sw $17 44($sp)
    li $8 2
    sw $8 12($sp)
    li $9 10
    addi $9 $9 1
    li $10 4
    mul $a0 $9 $10
    jal malloc
    addi $8 $v0 0
    li $11 10
    sw $11 0($8)
    sw $8 8($sp)
    li $12 100
    addi $13 $sp 8
    lw $14 0($13)
    li $15 1
    addi $16 $15 2
    addi $17 $16 1
    li $13 4
    mul $13 $17 $13
    add $15 $14 $13
    sw $12 0($15)
    addi $15 $sp 8
    lw $16 0($15)
    li $17 1
    addi $13 $17 2
    addi $15 $13 1
    li $13 4
    mul $13 $15 $13
    add $17 $16 $13
    lw $a0 0($17)
    jal print
    addi $8 $sp 8
    lw $9 0($8)
    lw $a0 0($9)
    jal print
    li $v0 0
    lw $8 8($sp)
    lw $9 12($sp)
    lw $10 16($sp)
    lw $11 20($sp)
    lw $12 24($sp)
    lw $13 28($sp)
    lw $14 32($sp)
    lw $15 36($sp)
    lw $16 40($sp)
    lw $17 44($sp)
    lw $ra 0($sp)
    addi $sp $sp 52
    jr $ra

