#
# MIPS assembly header for MiniJava code.  Defines the "built-in"
# functions, and does initialization for the memory allocator.  It
# then jumps to a procedure named "main" (so there had better be one),
# and exits once main returns.
#
        .data
        .align 2
_mem:   .space 102400       # 100K of heap space
_next:  .word 0
_nl:    .asciiz "\n"
_exit:  .asciiz "Exited with value "

        .text

        la   $s0, _mem 
        la   $s1, _next  
        sw   $s0, 0($s1)    # Set up ptr to start of malloc pool
        jal  main           # Jump to start of Minijava program
        nop
        li    $v0 10
        syscall             # syscall 10 (exit)  
        
#
# Implements the "built-in" print.  It expects a single integer
# argument to be passed in $a0.
#
print:  
        li   $v0, 1         # Specify the "print int" syscall
        syscall             # Arg is in $a0, so just do call
        la   $a0, _nl       # Load addr of \n string
        li   $v0, 4         # Specify the "print string" syscall
        syscall             # Print the string
        jr   $ra        
        
#
# Implements the "built-in" exit.  It expects a single integer
# argument to be passed in $a0.
#
exit:  
        move $s0, $a0       # Store the integer arg 
        la   $a0, _exit     # Load addr of "exit" string
        li   $v0, 4         # Specify the "print string" syscall
        syscall             # Print the string
        move $a0, $s0       # Set up the integer arg for printing
        li   $v0, 1         # Specify the "print int" syscall
        syscall             # Print the integer
        la   $a0, _nl       # Load addr of \n string
        li   $v0, 4         # Specify the "print string" syscall
        syscall             # Print the \n
        li    $v0 10        # Specify the MIPS exit syscall
        syscall             # exit

#
# Implements a quick and dirty "malloc" that draws from a fixed-size
# pool of memory, and never frees or reallocates memory.  Expects a
# single integer argument to be passed in $a0.  Written so that it
# uses only $a and $v registers and therefore needs no stack frame.
# (Look into into sbrk as a better way to allocate memory.)
#
malloc: 
        addi $a0, $a0, 3    # Round up to next word boundary
        srl  $a0, $a0, 2    # Remove lowest two bits by shifting
        sll  $a0, $a0, 2    #  right and then back to left
        la   $a1, _next     # Global pointing to free memory
        lw   $v0, 0($a1)    # Load its contents
        add  $v1, $v0, $a0  # Bump up to account for this chunk
        sw   $v1, 0($a1)    # Store new value back in global
        jr   $ra


Foo.printAll:
    subi $sp $sp 84
    sw $ra 0($sp)
    sw $gp 4($sp)
    sw $11 8($sp)
    sw $12 12($sp)
    sw $13 16($sp)
    sw $14 20($sp)
    sw $15 24($sp)
    sw $16 28($sp)
    sw $17 32($sp)
    sw $18 36($sp)
    sw $19 40($sp)
    sw $20 44($sp)
    sw $21 48($sp)
    sw $22 52($sp)
    sw $23 56($sp)
    sw $24 60($sp)
    sw $25 64($sp)
    sw $8 68($sp)
    sw $9 72($sp)
    sw $10 76($sp)
    addi $11 $sp 4
    lw $13 0($11)
    lw $14 0($13)
    li $15 0
    addi $16 $15 1
    li $17 4
    mul $17 $16 $17
    add $15 $14 $17
    lw $a0 0($15)
    jal print
    addi $19 $sp 4
    lw $21 0($19)
    lw $22 0($21)
    li $23 1
    addi $24 $23 1
    li $25 4
    mul $25 $24 $25
    add $23 $22 $25
    lw $a0 0($23)
    jal print
    addi $9 $sp 4
    lw $11 0($9)
    lw $12 0($11)
    li $13 2
    addi $14 $13 1
    li $15 4
    mul $15 $14 $15
    add $13 $12 $15
    lw $a0 0($13)
    jal print
    addi $17 $sp 4
    lw $19 0($17)
    lw $20 0($19)
    li $21 3
    addi $22 $21 1
    li $23 4
    mul $23 $22 $23
    add $21 $20 $23
    lw $a0 0($21)
    jal print
    addi $25 $sp 4
    lw $8 0($25)
    lw $9 0($8)
    li $10 4
    addi $11 $10 1
    li $12 4
    mul $12 $11 $12
    add $9 $9 $12
    lw $a0 0($9)
    jal print
    li $v0 0
    lw $11 8($sp)
    lw $12 12($sp)
    lw $13 16($sp)
    lw $14 20($sp)
    lw $15 24($sp)
    lw $16 28($sp)
    lw $17 32($sp)
    lw $18 36($sp)
    lw $19 40($sp)
    lw $20 44($sp)
    lw $21 48($sp)
    lw $22 52($sp)
    lw $23 56($sp)
    lw $24 60($sp)
    lw $25 64($sp)
    lw $8 68($sp)
    lw $9 72($sp)
    lw $10 76($sp)
    lw $ra 0($sp)
    addi $sp $sp 84
    jr $ra

main:
    sw  $ra, 0($sp)
    li $a0 4
    jal malloc
    addi $gp $v0 0
    li $16 0
    sw $16 4($gp)
    addi $gp $gp 0
    jal Foo.run
    addi $a0 $v0 0
    jal print
    lw  $ra, 0($sp)
    jr $ra

Foo.setAll:
    subi $sp $sp 84
    sw $ra 0($sp)
    sw $gp 4($sp)
    sw $24 8($sp)
    sw $25 12($sp)
    sw $8 16($sp)
    sw $9 20($sp)
    sw $10 24($sp)
    sw $11 28($sp)
    sw $12 32($sp)
    sw $13 36($sp)
    sw $14 40($sp)
    sw $15 44($sp)
    sw $16 48($sp)
    sw $17 52($sp)
    sw $18 56($sp)
    sw $19 60($sp)
    sw $20 64($sp)
    sw $21 68($sp)
    sw $22 72($sp)
    sw $23 76($sp)
    li $24 0
    addi $25 $sp 4
    lw $8 0($25)
    lw $9 0($8)
    li $10 0
    addi $11 $10 1
    li $12 4
    mul $12 $11 $12
    add $9 $9 $12
    sw $24 0($9)
    li $13 1
    addi $14 $sp 4
    lw $16 0($14)
    lw $17 0($16)
    li $18 1
    addi $19 $18 1
    li $20 4
    mul $20 $19 $20
    add $18 $17 $20
    sw $13 0($18)
    li $21 4
    addi $22 $sp 4
    lw $24 0($22)
    lw $25 0($24)
    li $8 2
    addi $8 $8 1
    li $9 4
    mul $8 $8 $9
    add $8 $25 $8
    sw $21 0($8)
    li $10 2
    addi $11 $sp 4
    lw $13 0($11)
    lw $14 0($13)
    li $15 3
    addi $16 $15 1
    li $17 4
    mul $17 $16 $17
    add $15 $14 $17
    sw $10 0($15)
    li $18 100
    addi $19 $sp 4
    lw $21 0($19)
    lw $22 0($21)
    li $23 4
    addi $24 $23 1
    li $25 4
    mul $25 $24 $25
    add $23 $22 $25
    sw $18 0($23)
    li $v0 0
    lw $24 8($sp)
    lw $25 12($sp)
    lw $8 16($sp)
    lw $9 20($sp)
    lw $10 24($sp)
    lw $11 28($sp)
    lw $12 32($sp)
    lw $13 36($sp)
    lw $14 40($sp)
    lw $15 44($sp)
    lw $16 48($sp)
    lw $17 52($sp)
    lw $18 56($sp)
    lw $19 60($sp)
    lw $20 64($sp)
    lw $21 68($sp)
    lw $22 72($sp)
    lw $23 76($sp)
    lw $ra 0($sp)
    addi $sp $sp 84
    jr $ra

Foo.run:
    subi $sp $sp 84
    sw $ra 0($sp)
    sw $gp 4($sp)
    sw $8 8($sp)
    sw $9 12($sp)
    sw $10 16($sp)
    sw $11 20($sp)
    sw $12 24($sp)
    sw $13 28($sp)
    sw $14 32($sp)
    sw $15 36($sp)
    sw $16 40($sp)
    sw $17 44($sp)
    sw $18 48($sp)
    sw $19 52($sp)
    sw $20 56($sp)
    sw $21 60($sp)
    sw $22 64($sp)
    sw $23 68($sp)
    sw $24 72($sp)
    sw $25 76($sp)
    li $8 5
    addi $8 $8 1
    li $9 4
    mul $a0 $8 $9
    jal malloc
    addi $8 $v0 0
    li $10 5
    sw $10 0($8)
    addi $11 $sp 4
    lw $12 0($11)
    sw $8 0($12)
    addi $13 $sp 4
    lw $gp 0($13)
    jal Foo.setAll
    sw $v0 8($sp)
    addi $15 $sp 4
    lw $gp 0($15)
    jal Foo.sumAll
    addi $a0 $v0 0
    jal print
    addi $17 $sp 4
    lw $gp 0($17)
    jal Foo.printAll
    sw $v0 8($sp)
    addi $19 $sp 4
    lw $21 0($19)
    lw $22 0($21)
    addi $23 $sp 4
    lw $25 0($23)
    lw $8 0($25)
    addi $9 $sp 4
    lw $11 0($9)
    lw $12 0($11)
    li $13 3
    addi $14 $13 1
    li $15 4
    mul $15 $14 $15
    add $13 $12 $15
    lw $16 0($13)
    addi $17 $16 1
    li $18 4
    mul $18 $17 $18
    add $8 $8 $18
    lw $19 0($8)
    addi $20 $19 1
    li $21 4
    mul $21 $20 $21
    add $23 $22 $21
    lw $a0 0($23)
    jal print
    li $v0 0
    lw $8 8($sp)
    lw $9 12($sp)
    lw $10 16($sp)
    lw $11 20($sp)
    lw $12 24($sp)
    lw $13 28($sp)
    lw $14 32($sp)
    lw $15 36($sp)
    lw $16 40($sp)
    lw $17 44($sp)
    lw $18 48($sp)
    lw $19 52($sp)
    lw $20 56($sp)
    lw $21 60($sp)
    lw $22 64($sp)
    lw $23 68($sp)
    lw $24 72($sp)
    lw $25 76($sp)
    lw $ra 0($sp)
    addi $sp $sp 84
    jr $ra

Foo.sumAll:
    subi $sp $sp 84
    sw $ra 0($sp)
    sw $gp 4($sp)
    sw $9 8($sp)
    sw $10 12($sp)
    sw $11 16($sp)
    sw $12 20($sp)
    sw $13 24($sp)
    sw $14 28($sp)
    sw $15 32($sp)
    sw $16 36($sp)
    sw $17 40($sp)
    sw $18 44($sp)
    sw $19 48($sp)
    sw $20 52($sp)
    sw $21 56($sp)
    sw $22 60($sp)
    sw $23 64($sp)
    sw $24 68($sp)
    sw $25 72($sp)
    sw $8 76($sp)
    addi $9 $sp 4
    lw $11 0($9)
    lw $12 0($11)
    li $13 0
    addi $14 $13 1
    li $15 4
    mul $15 $14 $15
    add $13 $12 $15
    lw $16 0($13)
    addi $17 $sp 4
    lw $19 0($17)
    lw $20 0($19)
    li $21 1
    addi $22 $21 1
    li $23 4
    mul $23 $22 $23
    add $21 $20 $23
    lw $24 0($21)
    add $17 $16 $24
    addi $25 $sp 4
    lw $8 0($25)
    lw $9 0($8)
    li $10 2
    addi $11 $10 1
    li $12 4
    mul $12 $11 $12
    add $9 $9 $12
    lw $13 0($9)
    add $25 $17 $13
    addi $14 $sp 4
    lw $16 0($14)
    lw $17 0($16)
    li $18 3
    addi $19 $18 1
    li $20 4
    mul $20 $19 $20
    add $18 $17 $20
    lw $21 0($18)
    add $14 $25 $21
    addi $22 $sp 4
    lw $24 0($22)
    lw $25 0($24)
    li $8 4
    addi $8 $8 1
    li $9 4
    mul $8 $8 $9
    add $8 $25 $8
    lw $10 0($8)
    add $v0 $14 $10
    lw $9 8($sp)
    lw $10 12($sp)
    lw $11 16($sp)
    lw $12 20($sp)
    lw $13 24($sp)
    lw $14 28($sp)
    lw $15 32($sp)
    lw $16 36($sp)
    lw $17 40($sp)
    lw $18 44($sp)
    lw $19 48($sp)
    lw $20 52($sp)
    lw $21 56($sp)
    lw $22 60($sp)
    lw $23 64($sp)
    lw $24 68($sp)
    lw $25 72($sp)
    lw $8 76($sp)
    lw $ra 0($sp)
    addi $sp $sp 84
    jr $ra

