#
# MIPS assembly header for MiniJava code.  Defines the "built-in"
# functions, and does initialization for the memory allocator.  It
# then jumps to a procedure named "main" (so there had better be one),
# and exits once main returns.
#
        .data
        .align 2
_mem:   .space 102400       # 100K of heap space
_next:  .word 0
_nl:    .asciiz "\n"
_exit:  .asciiz "Exited with value "

        .text

        la   $s0, _mem 
        la   $s1, _next  
        sw   $s0, 0($s1)    # Set up ptr to start of malloc pool
        jal  main           # Jump to start of Minijava program
        nop
        li    $v0 10
        syscall             # syscall 10 (exit)  
        
#
# Implements the "built-in" print.  It expects a single integer
# argument to be passed in $a0.
#
print:  
        li   $v0, 1         # Specify the "print int" syscall
        syscall             # Arg is in $a0, so just do call
        la   $a0, _nl       # Load addr of \n string
        li   $v0, 4         # Specify the "print string" syscall
        syscall             # Print the string
        jr   $ra        
        
#
# Implements the "built-in" exit.  It expects a single integer
# argument to be passed in $a0.
#
exit:  
        move $s0, $a0       # Store the integer arg 
        la   $a0, _exit     # Load addr of "exit" string
        li   $v0, 4         # Specify the "print string" syscall
        syscall             # Print the string
        move $a0, $s0       # Set up the integer arg for printing
        li   $v0, 1         # Specify the "print int" syscall
        syscall             # Print the integer
        la   $a0, _nl       # Load addr of \n string
        li   $v0, 4         # Specify the "print string" syscall
        syscall             # Print the \n
        li    $v0 10        # Specify the MIPS exit syscall
        syscall             # exit

#
# Implements a quick and dirty "malloc" that draws from a fixed-size
# pool of memory, and never frees or reallocates memory.  Expects a
# single integer argument to be passed in $a0.  Written so that it
# uses only $a and $v registers and therefore needs no stack frame.
# (Look into into sbrk as a better way to allocate memory.)
#
malloc: 
        addi $a0, $a0, 3    # Round up to next word boundary
        srl  $a0, $a0, 2    # Remove lowest two bits by shifting
        sll  $a0, $a0, 2    #  right and then back to left
        la   $a1, _next     # Global pointing to free memory
        lw   $v0, 0($a1)    # Load its contents
        add  $v1, $v0, $a0  # Bump up to account for this chunk
        sw   $v1, 0($a1)    # Store new value back in global
        jr   $ra


Fac.ComputeFac:
    subi $sp $sp 80
    sw $ra 0($sp)
    sw $gp 4($sp)
    sw $a0 8($sp)
    sw $8 12($sp)
    sw $9 16($sp)
    sw $10 20($sp)
    sw $11 24($sp)
    sw $12 28($sp)
    sw $13 32($sp)
    sw $21 36($sp)
    sw $22 40($sp)
    sw $23 44($sp)
    sw $14 48($sp)
    sw $15 52($sp)
    sw $16 56($sp)
    sw $17 60($sp)
    sw $18 64($sp)
    sw $19 68($sp)
    sw $20 72($sp)
    # <
    addi $8 $sp 8
    lw $9 0($8)
    li $10 1
    slt $11 $9 $10
    bne $11 $zero L3
    j L4

L0:
    li $21 1
    sw $21 12($sp)
    j L2

L2:
    addi $22 $sp 12
    lw $v0 0($22)
    lw $8 12($sp)
    lw $9 16($sp)
    lw $10 20($sp)
    lw $11 24($sp)
    lw $12 28($sp)
    lw $13 32($sp)
    lw $21 36($sp)
    lw $22 40($sp)
    lw $23 44($sp)
    lw $14 48($sp)
    lw $15 52($sp)
    lw $16 56($sp)
    lw $17 60($sp)
    lw $18 64($sp)
    lw $19 68($sp)
    lw $20 72($sp)
    lw $ra 0($sp)
    addi $sp $sp 80
    jr $ra

L3:
    li $12 1
    j L5

L5:
    li $13 1
    beq $12 $13 L0
    j L1

main:
    sw  $ra, 0($sp)
    li $gp 4
    li $a0 10
    jal Fac.ComputeFac
    addi $a0 $v0 0
    jal print
    lw  $ra, 0($sp)
    jr $ra

L1:
    addi $14 $sp 8
    lw $15 0($14)
    addi $16 $sp 4
    lw $gp 0($16)
    addi $18 $sp 8
    lw $19 0($18)
    subi $a0 $19 1
    jal Fac.ComputeFac
    mul $16 $15 $v0
    sw $16 12($sp)
    j L2

L4:
    li $12 0
    j L5

