#
# MIPS assembly header for MiniJava code.  Defines the "built-in"
# functions, and does initialization for the memory allocator.  It
# then jumps to a procedure named "main" (so there had better be one),
# and exits once main returns.
#
        .data
        .align 2
_mem:   .space 102400       # 100K of heap space
_next:  .word 0
_nl:    .asciiz "\n"
_exit:  .asciiz "Exited with value "

        .text

        la   $s0, _mem 
        la   $s1, _next  
        sw   $s0, 0($s1)    # Set up ptr to start of malloc pool
        jal  main           # Jump to start of Minijava program
        nop
        li    $v0 10
        syscall             # syscall 10 (exit)  
        
#
# Implements the "built-in" print.  It expects a single integer
# argument to be passed in $a0.
#
print:  
        li   $v0, 1         # Specify the "print int" syscall
        syscall             # Arg is in $a0, so just do call
        la   $a0, _nl       # Load addr of \n string
        li   $v0, 4         # Specify the "print string" syscall
        syscall             # Print the string
        jr   $ra        
        
#
# Implements the "built-in" exit.  It expects a single integer
# argument to be passed in $a0.
#
exit:  
        move $s0, $a0       # Store the integer arg 
        la   $a0, _exit     # Load addr of "exit" string
        li   $v0, 4         # Specify the "print string" syscall
        syscall             # Print the string
        move $a0, $s0       # Set up the integer arg for printing
        li   $v0, 1         # Specify the "print int" syscall
        syscall             # Print the integer
        la   $a0, _nl       # Load addr of \n string
        li   $v0, 4         # Specify the "print string" syscall
        syscall             # Print the \n
        li    $v0 10        # Specify the MIPS exit syscall
        syscall             # exit

#
# Implements a quick and dirty "malloc" that draws from a fixed-size
# pool of memory, and never frees or reallocates memory.  Expects a
# single integer argument to be passed in $a0.  Written so that it
# uses only $a and $v registers and therefore needs no stack frame.
# (Look into into sbrk as a better way to allocate memory.)
#
malloc: 
        addi $a0, $a0, 3    # Round up to next word boundary
        srl  $a0, $a0, 2    # Remove lowest two bits by shifting
        sll  $a0, $a0, 2    #  right and then back to left
        la   $a1, _next     # Global pointing to free memory
        lw   $v0, 0($a1)    # Load its contents
        add  $v1, $v0, $a0  # Bump up to account for this chunk
        sw   $v1, 0($a1)    # Store new value back in global
        jr   $ra


L15:
    li $14 1
    j L17

L0:
    # <
    addi $9 $sp 16
    lw $10 0($9)
    addi $11 $sp 12
    lw $12 0($11)
    slt $13 $10 $12
    bne $13 $zero L3
    j L4

L10:
    li $14 0
    j L11

L12:
    addi $16 $sp 36
    lw $17 0($16)
    subi $18 $17 1
    sw $18 28($sp)
    addi $19 $sp 4
    lw $21 0($19)
    lw $22 0($21)
    addi $23 $sp 28
    lw $24 0($23)
    addi $25 $24 1
    li $8 4
    mul $8 $25 $8
    add $23 $22 $8
    lw $9 0($23)
    sw $9 40($sp)
    addi $10 $sp 4
    lw $12 0($10)
    lw $13 0($12)
    addi $14 $sp 36
    lw $15 0($14)
    addi $16 $15 1
    li $17 4
    mul $17 $16 $17
    add $14 $13 $17
    lw $18 0($14)
    addi $19 $sp 4
    lw $21 0($19)
    lw $22 0($21)
    addi $23 $sp 28
    lw $24 0($23)
    addi $25 $24 1
    li $8 4
    mul $8 $25 $8
    add $23 $22 $8
    sw $18 0($23)
    addi $9 $sp 40
    lw $10 0($9)
    addi $11 $sp 4
    lw $13 0($11)
    lw $14 0($13)
    addi $15 $sp 36
    lw $16 0($15)
    addi $17 $16 1
    li $18 4
    mul $18 $17 $18
    add $15 $14 $18
    sw $10 0($15)
    j L14

BBS.Start:
    subi $sp $sp 20
    sw $ra 0($sp)
    sw $gp 4($sp)
    sw $a0 8($sp)
    sw $8 12($sp)
    addi $8 $sp 4
    lw $gp 0($8)
    addi $8 $sp 8
    lw $a0 0($8)
    jal BBS.Init
    sw $v0 12($sp)
    addi $8 $sp 4
    lw $gp 0($8)
    jal BBS.Print
    sw $v0 12($sp)
    li $a0 99999
    jal print
    addi $8 $sp 4
    lw $gp 0($8)
    jal BBS.Sort
    sw $v0 12($sp)
    addi $8 $sp 4
    lw $gp 0($8)
    jal BBS.Print
    sw $v0 12($sp)
    li $v0 0
    lw $8 12($sp)
    lw $ra 0($sp)
    addi $sp $sp 20
    jr $ra

L1:
    li $16 1
    sw $16 36($sp)

BBS.Sort:
    subi $sp $sp 20
    sw $ra 0($sp)
    sw $gp 4($sp)
    sw $20 8($sp)
    sw $8 12($sp)
    addi $20 $sp 8
    lw $20 0($20)
    lw $20 0($20)
    subi $20 $20 1
    sw $20 12($sp)
    subi $8 $20 1
    sw $8 16($sp)
    lw $20 8($sp)
    lw $8 12($sp)
    lw $ra 0($sp)
    addi $sp $sp 20
    jr $ra

L17:
    li $14 1
    beq $14 $14 L12
    j L13

L16:
    li $14 0
    j L17

BBS.Print:
    subi $sp $sp 16
    sw $ra 0($sp)
    sw $gp 4($sp)
    sw $8 8($sp)
    sw $8 8($sp)
    lw $8 8($sp)
    lw $ra 0($sp)
    addi $sp $sp 16
    jr $ra

L7:
    addi $24 $sp 36
    lw $25 0($24)
    subi $8 $25 1
    sw $8 32($sp)
    addi $9 $sp 4
    lw $11 0($9)
    lw $12 0($11)
    addi $13 $sp 32
    lw $14 0($13)
    addi $15 $14 1
    li $16 4
    mul $16 $15 $16
    add $13 $12 $16
    lw $17 0($13)
    sw $17 20($sp)
    addi $18 $sp 4
    lw $20 0($18)
    lw $21 0($20)
    addi $22 $sp 36
    lw $23 0($22)
    addi $24 $23 1
    li $25 4
    mul $25 $24 $25
    add $22 $21 $25
    lw $8 0($22)
    sw $8 24($sp)
    # <
    addi $9 $sp 24
    lw $10 0($9)
    addi $11 $sp 20
    lw $12 0($11)
    slt $13 $10 $12
    bne $13 $zero L15
    j L16

L19:
    addi $17 $sp 4
    lw $19 0($17)
    lw $20 0($19)
    addi $21 $sp 8
    lw $22 0($21)
    addi $23 $22 1
    li $24 4
    mul $24 $23 $24
    add $21 $20 $24
    lw $a0 0($21)
    jal print
    addi $8 $sp 8
    lw $9 0($8)
    addi $9 $9 1
    sw $9 8($sp)
    j L18

L20:
    li $v0 0

L11:
    li $23 0
    beq $14 $23 L8
    j L7

L13:
    li $15 0
    sw $15 8($sp)
    j L14

L14:
    addi $19 $sp 36
    lw $20 0($19)
    addi $21 $20 1
    sw $21 36($sp)
    j L6

L8:
    addi $22 $sp 12
    lw $23 0($22)
    subi $24 $23 1
    sw $24 12($sp)
    j L0

L9:
    li $14 1
    j L11

L21:
    li $14 1
    j L23

L18:
    # <
    addi $9 $sp 8
    lw $10 0($9)
    addi $11 $sp 8
    lw $13 0($11)
    lw $14 0($13)
    slt $15 $10 $14
    bne $15 $zero L21
    j L22

L4:
    li $14 0
    j L5

main:
    sw  $ra, 0($sp)
    li $a0 8
    jal malloc
    addi $gp $v0 0
    li $12 0
    sw $12 4($gp)
    li $13 0
    sw $13 8($gp)
    addi $gp $gp 0
    li $a0 10
    jal BBS.Start
    addi $a0 $v0 0
    jal print
    lw  $ra, 0($sp)
    jr $ra

L5:
    li $15 0
    beq $14 $15 L2
    j L1

L3:
    li $14 1
    j L5

L6:
    # <
    addi $17 $sp 36
    lw $18 0($17)
    addi $19 $sp 12
    lw $20 0($19)
    addi $21 $20 1
    slt $22 $18 $21
    bne $22 $zero L9
    j L10

L23:
    li $16 0
    beq $14 $16 L20
    j L19

L2:
    li $v0 0

L22:
    li $14 0
    j L23

BBS.Init:
    subi $sp $sp 88
    sw $ra 0($sp)
    sw $gp 4($sp)
    sw $a0 8($sp)
    sw $11 12($sp)
    sw $12 16($sp)
    sw $13 20($sp)
    sw $14 24($sp)
    sw $15 28($sp)
    sw $16 32($sp)
    sw $17 36($sp)
    sw $18 40($sp)
    sw $8 44($sp)
    sw $19 48($sp)
    sw $20 52($sp)
    sw $21 56($sp)
    sw $22 60($sp)
    sw $23 64($sp)
    sw $24 68($sp)
    sw $25 72($sp)
    sw $9 76($sp)
    sw $10 80($sp)
    addi $11 $sp 8
    lw $12 0($11)
    addi $13 $sp 8
    lw $14 0($13)
    sw $12 0($14)
    addi $15 $sp 8
    lw $16 0($15)
    addi $17 $16 1
    li $18 4
    mul $a0 $17 $18
    jal malloc
    addi $8 $v0 0
    addi $19 $sp 8
    lw $20 0($19)
    sw $20 0($8)
    addi $21 $sp 4
    lw $22 0($21)
    sw $8 0($22)
    li $23 20
    addi $24 $sp 4
    lw $8 0($24)
    lw $9 0($8)
    li $10 0
    addi $11 $10 1
    li $12 4
    mul $12 $11 $12
    add $9 $9 $12
    sw $23 0($9)
    li $13 7
    addi $14 $sp 4
    lw $16 0($14)
    lw $17 0($16)
    li $18 1
    addi $19 $18 1
    li $20 4
    mul $20 $19 $20
    add $18 $17 $20
    sw $13 0($18)
    li $21 12
    addi $22 $sp 4
    lw $24 0($22)
    lw $25 0($24)
    li $8 2
    addi $8 $8 1
    li $9 4
    mul $8 $8 $9
    add $8 $25 $8
    sw $21 0($8)
    li $10 18
    addi $11 $sp 4
    lw $13 0($11)
    lw $14 0($13)
    li $15 3
    addi $16 $15 1
    li $17 4
    mul $17 $16 $17
    add $15 $14 $17
    sw $10 0($15)
    li $18 2
    addi $19 $sp 4
    lw $21 0($19)
    lw $22 0($21)
    li $23 4
    addi $24 $23 1
    li $25 4
    mul $25 $24 $25
    add $23 $22 $25
    sw $18 0($23)
    li $8 11
    addi $9 $sp 4
    lw $11 0($9)
    lw $12 0($11)
    li $13 5
    addi $14 $13 1
    li $15 4
    mul $15 $14 $15
    add $13 $12 $15
    sw $8 0($13)
    li $16 6
    addi $17 $sp 4
    lw $19 0($17)
    lw $20 0($19)
    li $21 6
    addi $22 $21 1
    li $23 4
    mul $23 $22 $23
    add $21 $20 $23
    sw $16 0($21)
    li $24 9
    addi $25 $sp 4
    lw $8 0($25)
    lw $9 0($8)
    li $10 7
    addi $11 $10 1
    li $12 4
    mul $12 $11 $12
    add $9 $9 $12
    sw $24 0($9)
    li $13 19
    addi $14 $sp 4
    lw $16 0($14)
    lw $17 0($16)
    li $18 8
    addi $19 $18 1
    li $20 4
    mul $20 $19 $20
    add $18 $17 $20
    sw $13 0($18)
    li $21 5
    addi $22 $sp 4
    lw $24 0($22)
    lw $25 0($24)
    li $8 9
    addi $8 $8 1
    li $9 4
    mul $8 $8 $9
    add $8 $25 $8
    sw $21 0($8)
    li $v0 0
    lw $11 12($sp)
    lw $12 16($sp)
    lw $13 20($sp)
    lw $14 24($sp)
    lw $15 28($sp)
    lw $16 32($sp)
    lw $17 36($sp)
    lw $18 40($sp)
    lw $8 44($sp)
    lw $19 48($sp)
    lw $20 52($sp)
    lw $21 56($sp)
    lw $22 60($sp)
    lw $23 64($sp)
    lw $24 68($sp)
    lw $25 72($sp)
    lw $9 76($sp)
    lw $10 80($sp)
    lw $ra 0($sp)
    addi $sp $sp 88
    jr $ra

