#
# MIPS assembly header for MiniJava code.  Defines the "built-in"
# functions, and does initialization for the memory allocator.  It
# then jumps to a procedure named "main" (so there had better be one),
# and exits once main returns.
#
        .data
        .align 2
_mem:   .space 102400       # 100K of heap space
_next:  .word 0
_nl:    .asciiz "\n"
_exit:  .asciiz "Exited with value "

        .text

        la   $s0, _mem 
        la   $s1, _next  
        sw   $s0, 0($s1)    # Set up ptr to start of malloc pool
        jal  main           # Jump to start of Minijava program
        nop
        li    $v0 10
        syscall             # syscall 10 (exit)  
        
#
# Implements the "built-in" print.  It expects a single integer
# argument to be passed in $a0.
#
print:  
        li   $v0, 1         # Specify the "print int" syscall
        syscall             # Arg is in $a0, so just do call
        la   $a0, _nl       # Load addr of \n string
        li   $v0, 4         # Specify the "print string" syscall
        syscall             # Print the string
        jr   $ra        
        
#
# Implements the "built-in" exit.  It expects a single integer
# argument to be passed in $a0.
#
exit:  
        move $s0, $a0       # Store the integer arg 
        la   $a0, _exit     # Load addr of "exit" string
        li   $v0, 4         # Specify the "print string" syscall
        syscall             # Print the string
        move $a0, $s0       # Set up the integer arg for printing
        li   $v0, 1         # Specify the "print int" syscall
        syscall             # Print the integer
        la   $a0, _nl       # Load addr of \n string
        li   $v0, 4         # Specify the "print string" syscall
        syscall             # Print the \n
        li    $v0 10        # Specify the MIPS exit syscall
        syscall             # exit

#
# Implements a quick and dirty "malloc" that draws from a fixed-size
# pool of memory, and never frees or reallocates memory.  Expects a
# single integer argument to be passed in $a0.  Written so that it
# uses only $a and $v registers and therefore needs no stack frame.
# (Look into into sbrk as a better way to allocate memory.)
#
malloc: 
        addi $a0, $a0, 3    # Round up to next word boundary
        srl  $a0, $a0, 2    # Remove lowest two bits by shifting
        sll  $a0, $a0, 2    #  right and then back to left
        la   $a1, _next     # Global pointing to free memory
        lw   $v0, 0($a1)    # Load its contents
        add  $v1, $v0, $a0  # Bump up to account for this chunk
        sw   $v1, 0($a1)    # Store new value back in global
        jr   $ra



Foo.get:
    subi $sp $sp 16
    sw $ra 0($sp)
    sw $gp 4($sp)
    sw $9 8($sp)
    addi $9 $sp 4
    lw $9 0($9)
    lw $v0 0($9)
    lw $9 8($sp)
    lw $ra 0($sp)
    addi $sp $sp 16
    jr $ra

main:
    sw  $ra, 0($sp)
    li $gp 4
    jal Run.test
    addi $a0 $v0 0
    jal print
    lw  $ra, 0($sp)
    jr $ra

Foo.set:
    subi $sp $sp 28
    sw $ra 0($sp)
    sw $gp 4($sp)
    sw $a0 8($sp)
    sw $21 12($sp)
    sw $22 16($sp)
    sw $8 20($sp)
    addi $21 $sp 8
    lw $22 0($21)
    addi $21 $sp 4
    lw $21 0($21)
    sw $22 0($21)
    addi $21 $sp 8
    lw $v0 0($21)
    lw $21 12($sp)
    lw $22 16($sp)
    lw $8 20($sp)
    lw $ra 0($sp)
    addi $sp $sp 28
    jr $ra

Run.test:
    subi $sp $sp 60
    sw $ra 0($sp)
    sw $gp 4($sp)
    sw $8 8($sp)
    sw $9 12($sp)
    sw $10 16($sp)
    sw $11 20($sp)
    sw $12 24($sp)
    sw $13 28($sp)
    sw $14 32($sp)
    sw $15 36($sp)
    sw $16 40($sp)
    sw $17 44($sp)
    sw $18 48($sp)
    sw $19 52($sp)
    li $a0 4
    jal malloc
    addi $gp $v0 0
    li $9 0
    sw $9 4($gp)
    sw $gp 8($sp)
    li $a0 8
    jal malloc
    addi $gp $v0 0
    li $11 0
    sw $11 4($gp)
    li $12 0
    sw $12 4($gp)
    sw $gp 12($sp)
    addi $13 $sp 8
    lw $gp 0($13)
    li $a0 5
    jal Foo.set
    sw $v0 16($sp)
    addi $16 $sp 12
    lw $gp 0($16)
    li $a0 50
    jal Foo.set
    sw $v0 16($sp)
    addi $19 $sp 12
    lw $gp 0($19)
    li $a0 10
    jal SubFoo.setMyVar
    sw $v0 16($sp)
    addi $19 $sp 8
    lw $gp 0($19)
    jal Foo.get
    addi $a0 $v0 0
    jal print
    addi $19 $sp 12
    lw $gp 0($19)
    jal SubFoo.get
    addi $a0 $v0 0
    jal print
    addi $8 $sp 12
    lw $gp 0($8)
    jal SubFoo.getMyVar
    addi $a0 $v0 0
    jal print
    addi $10 $sp 4
    lw $gp 0($10)
    addi $12 $sp 8
    lw $a0 0($12)
    jal Run.printFooVar
    sw $v0 16($sp)
    addi $14 $sp 4
    lw $gp 0($14)
    addi $16 $sp 12
    lw $a0 0($16)
    jal Run.printFooVar
    sw $v0 16($sp)
    li $v0 0
    lw $8 8($sp)
    lw $9 12($sp)
    lw $10 16($sp)
    lw $11 20($sp)
    lw $12 24($sp)
    lw $13 28($sp)
    lw $14 32($sp)
    lw $15 36($sp)
    lw $16 40($sp)
    lw $17 44($sp)
    lw $18 48($sp)
    lw $19 52($sp)
    lw $ra 0($sp)
    addi $sp $sp 60
    jr $ra

SubFoo.setMyVar:
    subi $sp $sp 24
    sw $ra 0($sp)
    sw $gp 4($sp)
    sw $a0 8($sp)
    sw $24 12($sp)
    sw $8 16($sp)
    addi $24 $sp 8
    lw $24 0($24)
    addi $8 $sp 4
    lw $8 0($8)
    sw $24 4($8)
    addi $8 $sp 8
    lw $v0 0($8)
    lw $24 12($sp)
    lw $8 16($sp)
    lw $ra 0($sp)
    addi $sp $sp 24
    jr $ra

SubFoo.getMyVar:
    subi $sp $sp 16
    sw $ra 0($sp)
    sw $gp 4($sp)
    sw $17 8($sp)
    addi $17 $sp 4
    lw $17 0($17)
    addi $17 $17 4
    lw $v0 0($17)
    lw $17 8($sp)
    lw $ra 0($sp)
    addi $sp $sp 16
    jr $ra

SubFoo.get:
    subi $sp $sp 24
    sw $ra 0($sp)
    sw $gp 4($sp)
    sw $12 8($sp)
    sw $13 12($sp)
    sw $14 16($sp)
    li $12 2
    addi $13 $sp 4
    lw $14 0($13)
    lw $14 0($14)
    mul $v0 $12 $14
    lw $12 8($sp)
    lw $13 12($sp)
    lw $14 16($sp)
    lw $ra 0($sp)
    addi $sp $sp 24
    jr $ra

Run.printFooVar:
    subi $sp $sp 20
    sw $ra 0($sp)
    sw $gp 4($sp)
    sw $a0 8($sp)
    sw $19 12($sp)
    addi $19 $sp 8
    lw $gp 0($19)
    jal Foo.get
    sw $v0 12($sp)
    addi $19 $sp 12
    lw $a0 0($19)
    jal print
    li $v0 0
    lw $19 12($sp)
    lw $ra 0($sp)
    addi $sp $sp 20
    jr $ra

