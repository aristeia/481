package Tree;

/** 
 * A pretty-printer for IRT code.  The IRT node classes aren't set up to
 * allow visitors, so we traverse the tree the "old fashioned" way:  Use
 * instanceof to figure out which Stm or Exp we're dealing with, and jump
 * to a method that knows how to print the particular node.  In general,
 * you'll use Print.prStm() to print an IRT program.  If you've got an IRT
 * fragment whose root is an Exp node, you'll need to use Print.prExp().
 */

public class PrintToString {  
   
   /**
    * Top-level call to start the traversal and printing of an IRT tree
    * whose root node is a statement.
    * @param s  The root (statement node) of an IRT tree
    */
    public static String prStm(Stm s) {return prStm(s,0)+"\n";}
   
   /**
    * Top-level call to start the traversal and printing of an IRT tree
    * whose root node is an expression.
    * @param e  The root (expression node) of an IRT tree
    */
   public static String prExp(Exp e) {return prExp(e,0)+"\n";}
   
   public static String indent(int d) {
       String ret = "";
       for(int i=0; i<d; i++) 
	   ret+=' ';
       return ret;
   }
   
   public static String say(String s) {
      return s;
   }
   
   public static String sayln(String s) {
       return say(s)+say("\n");
   }
   
   public static String prStm(SEQ s, int d) {
      return indent(d)+sayln("SEQ(")+prStm(s.left,d+1)+sayln(",")+prStm(s.right,d+1)+say(")");
   }
   
   public static String prStm(LABEL s, int d) {
      return indent(d)+say("LABEL ")+say(s.label.toString());
   }
      
   public static String prStm(COMMENT s, int d) {
      return indent(d)+say("COMMENT:  "+s.text);
   }
   
   public static String prStm(JUMP s, int d) {
      return indent(d)+ sayln("JUMP(")+ prExp(s.exp, d+1)+ say(")");
   }
   
   public static String prStm(CJUMP s, int d) {
      String ret = indent(d)+ say("CJUMP("); 
      switch(s.relop) {
         case CJUMP.EQ: ret+=say("EQ"); break;
         case CJUMP.NE: ret+=say("NE"); break;
         case CJUMP.LT: ret+=say("LT"); break;
         case CJUMP.GT: ret+=say("GT"); break;
         case CJUMP.LE: ret+=say("LE"); break;
         case CJUMP.GE: ret+=say("GE"); break;
         case CJUMP.ULT: ret+=say("ULT"); break;
         case CJUMP.ULE: ret+=say("ULE"); break;
         case CJUMP.UGT: ret+=say("UGT"); break;
         case CJUMP.UGE: ret+=say("UGE"); break;
         default:
            return ret+"Print.prStm.CJUMP";
      }
      ret+=sayln(",")+ prExp(s.left,d+1)+ sayln(",")+ prExp(s.right,d+1)+ sayln(",")+indent(d+1)+ say(s.iftrue.toString())+ say(","); 
      return ret+say(s.iffalse.toString())+say(")");
   }
   
   public static String prStm(MOVE s, int d) {
       return indent(d)+ sayln("MOVE(")+ prExp(s.dst,d+1)+ sayln(",")+ prExp(s.src,d+1)+ say(")");
   }
   
   public static String prStm(EXPR s, int d) {
      return indent(d)+ sayln("EXPR(")+ prExp(s.exp,d+1)+ say(")"); 
   }
   
   public static String prStm(RETURN s, int d) {
      return indent(d)+sayln("RETURN(")+ prExp(s.ret,d+1)+ say(")"); 
   }
   
   public static String prStm(Stm s, int d) {
      if (s instanceof SEQ) return prStm((SEQ)s, d);
      else if (s instanceof LABEL) return prStm((LABEL)s, d);
      else if (s instanceof COMMENT) return prStm((COMMENT)s, d);
      else if (s instanceof JUMP) return prStm((JUMP)s, d);
      else if (s instanceof CJUMP) return prStm((CJUMP)s, d);
      else if (s instanceof MOVE) return prStm((MOVE)s, d);
      else if (s instanceof EXPR) return prStm((EXPR)s, d);
      else if (s instanceof RETURN) return prStm((RETURN)s, d);
      else return "Print.prStm";
   }
   
   public static String prExp(BINOP e, int d) {
      String ret=indent(d)+ say("BINOP("); 
      switch(e.binop) {
         case BINOP.PLUS: ret+=say("PLUS"); break;
         case BINOP.MINUS: ret+=say("MINUS"); break;
         case BINOP.MUL: ret+=say("MUL"); break;
         case BINOP.DIV: ret+=say("DIV"); break;
         case BINOP.AND: ret+=say("AND"); break;
         case BINOP.OR:ret+= say("OR"); break;
         case BINOP.LSHIFT:ret+= say("LSHIFT"); break;
         case BINOP.RSHIFT:ret+= say("RSHIFT"); break;
         case BINOP.ARSHIFT:ret+= say("ARSHIFT"); break;
         case BINOP.XOR: ret+=say("XOR"); break;
         default:
            return ret+"Print.prExp.BINOP";
      }
      ret+=sayln(",");
      return ret+prExp(e.left,d+1)+ sayln(",")+ prExp(e.right,d+1)+ say(")");
   }
   
   public static String prExp(MEM e, int d) {
      return indent(d)+ sayln("MEM(")+ prExp(e.exp,d+1)+ say(")");
   }
   
   public static String prExp(REG e, int d) {
      return indent(d)+ say("REG ")+say(e.reg.toString());
   }
   
   public static String prExp(ESEQ e, int d) {
      return indent(d)+sayln("ESEQ(")+ prStm(e.stm,d+1)+ sayln(",")+ prExp(e.exp,d+1)+ say(")");
      
   }
   
   public static String prExp(NAME e, int d) {
      return indent(d)+say("NAME ")+ say(e.label.toString());
   }
   
   public static String prExp(CONST e, int d) {
      return indent(d)+ say("CONST ")+ say(String.valueOf(e.value));
   }
   
   public static String prExp(CALL e, int d) {
      String ret = indent(d)+ sayln("CALL(")+ prExp(e.func,d+1);
      for(ExpList a = e.args; a!=null; a=a.tail) {
         ret+=sayln(",")+ prExp(a.head,d+2); 
      }
      return ret+say(")");
   }
   
   public static String prExp(Exp e, int d) {
      if (e instanceof BINOP) return prExp((BINOP)e, d);
      else if (e instanceof MEM) return prExp((MEM)e, d);
      else if (e instanceof REG) return prExp((REG)e, d);
      else if (e instanceof ESEQ) return prExp((ESEQ)e, d);
      else if (e instanceof NAME) return prExp((NAME)e, d);
      else if (e instanceof CONST) return prExp((CONST)e, d);
      else if (e instanceof CALL) return prExp((CALL)e, d);
      else return "Print.prExp";
   }
   
}
