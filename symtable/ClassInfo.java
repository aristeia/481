package symtable;

import java.util.*;

import types.Types;
import minijava.node.PMethod;
import minijava.node.PVarDecl;
import minijava.node.AVarDecl;
import minijava.node.TId;

import Mips.*;  // These two are needed for the IRT phase
import Arch.*;
import Tree.*;

/** 
 * A ClassInfo instance records infofmation about a single class.  It stores 
 * the name of its superclass (or null if there isn't one), a VarTable containing 
 * the class's instance variables, and a MethodTable containing information on 
 * the methods in the class, in addition to the name of the class itself.
 * 
 * @author Brad Richards
 */
 
public class ClassInfo {
   
   private TId className;         // TId holding our name, line number, etc.
   private TId superClass;        // Our superclass, if we have one
   private VarTable vars;         // A VarTable holding info on all instance vars
   private MethodTable methods;   // Table of info on methods
   private ClassInfo sup;

    /*
   We'll add these once we get to the IRT phase.  The IRTinfo object records
   the total number of words required for the instance variables in a class
   (including those we inherit).  
    */
   private Access info;
   public Access getIRTinfo() { return info; }
   public void setIRTinfo(Access i) { info = i; }


   /** 
    * The constructor takes all info associated with a subclass definition,
    * but can be passed null for unused fields in the case of a base or main
    * class declaration.  Names are passed as TId rather than String so we 
    * can retrieve line number, etc, from the token if necessary.
    * @param className  The name of the class
    * @param superClass The name of its superclass
    * @param vars       A list of all instance vars in the class
    * @param methods    A list of method descriptors
    */
   public ClassInfo(TId className, TId superClass,
                    LinkedList<PVarDecl> vars,
                    LinkedList<PMethod> methods) throws Exception { 

    
       this.className = className;
       this.superClass = superClass;
       try {
	   this.vars = new VarTable(vars);
       }
       catch(VarClashException e){
	   throw e;
       } 
       
       /*
	 this.info = new InFrame(0);
	 this.vars = new VarTable(null);
	 int i=0;
	 for (PVarDecl pp: vars){
	 AVarDecl p = (AVarDecl)pp.clone();
	 p.setId(new TId(p.getId().getText().replaceAll("\\s","")));
	 try {
	 this.vars.put(p.getId(), p.getType());
	 this.vars.getInfo(p.getId().getText()).setAccess(new InFrame(4*i++));
	 }
	 catch(VarClashException e){
	 throw e;
	 } }	  */
       try {
	   this.methods = new MethodTable(methods);
       }  // Ditto.
       catch(Exception e){
	   System.out.println("throwing methods");
	   throw e;
	   
       }
   }

   public void addSuper(ClassInfo s){
    this.sup = s;
   }
    
    public void allocateMem(){
	this.info = new InFrame(4);
	// System.out.println(this.getName().toString() + " -- alloc mem");
	String[] tempKeys = new String[this.vars.getVarNames().size()];
	this.vars.getVarNames().toArray(tempKeys);
	for(int i=1; i<vars.size()+1; i++){
	    Access temp = new InFrame((i*4));
	    this.vars.getInfo(tempKeys[i-1]).setAccess(temp);
	}
    }

    public void allocateMemWithSuper(){
      
      this.info = new InFrame(4);
      int numVars = this.vars.getVarNames().size();// + this.sup.getVarTable().getVarNames().size();
      String[] tempKeys = new String[numVars];
      Set<String> superVars = this.sup.getVarTable().getVarNames();
      Set<String> sub = this.vars.getVarNames();
     int i = 0;
     /*for(String a : superVars){
         tempKeys[i] = a;
        i++;
	}*/
     
      for(String a : sub){
      tempKeys[i] = a;
        i++;
      }  
      for( i=0; i < tempKeys.length; i++){
          Access temp = new InFrame((i*4)+4);
          if (this.vars.getInfo(tempKeys[i]) != null)
            this.vars.getInfo(tempKeys[i]).setAccess(temp);
          else
            this.sup.getVarTable().getInfo(tempKeys[i]).setAccess(temp);
      }
    }
    



   public TId getName() { return className; }
   public TId getSuper() { return superClass; }
   public VarTable getVarTable() { return vars; }
   public MethodTable getMethodTable() { return methods; }
   
   public void dump() {
       String s = className.getText();
       if(superClass != null) 
         s+="  Extends: "+getSuper().getText();
       
       System.out.println("-------------------------------------");
       System.out.println("Class: " +s);
       System.out.println("-------------------------------------");
       vars.dump();
       methods.dump();
   }


   public Exp genIRT(){
       REG dest = new REG(new Reg("$gp"));
       if (this.vars.getVarNames().size()==0){
	   MOVE static_link = new MOVE(dest, ((InFrame)info).getTree());
	   ESEQ eseq = new ESEQ(static_link,dest);
	   return (Exp) eseq;
       }
       else {
	   String[] tempKeys;
	   if(superClass != null){
	       int numVars = this.vars.getVarNames().size() + this.sup.getVarTable().getVarNames().size();
	       tempKeys = new String[numVars];
	       Set<String> superVars = this.sup.getVarTable().getVarNames();
	       Set<String> sub = this.vars.getVarNames();
	       int i = 0;
	       for(String a : sub){
		   tempKeys[i] = a;
		   i++;
	       }
	       for(String a : superVars){
		   tempKeys[i] = a;
		   i++;
	       }
	   }
	   else {
	       int numVars = this.vars.getVarNames().size();
	       tempKeys = new String[numVars];
	       this.vars.getVarNames().toArray(tempKeys);
	   }
	   
	   
	   ExpList el = new ExpList(new CONST(tempKeys.length*4), null); 
	   
	   MOVE malloc = new MOVE(dest, new CALL(new NAME( new Label("malloc")),el  ));
	   Stm temp;
	   if (this.vars.getInfo(tempKeys[0]) != null)
	       temp = new SEQ(malloc, new MOVE(new MEM(new BINOP(0, new REG(new Reg("$gp")), this.vars.getInfo(tempKeys[0]).getAccess().getTree())),new CONST(0)) );
	   else
	       temp = new SEQ(malloc, new MOVE(new MEM(new BINOP(0, new REG(new Reg("$gp")), this.sup.getVarTable().getInfo(tempKeys[0]).getAccess().getTree())),new CONST(0)) );
	   
	   for(int i=1;i<tempKeys.length;i++)
	       if (this.vars.getInfo(tempKeys[i]) != null)
		   temp = new SEQ(temp, new MOVE(new MEM(new BINOP(0, new REG(new Reg("$gp")), this.vars.getInfo(tempKeys[i]).getAccess().getTree())),new CONST(0)) );          
	       else
		   temp = new SEQ(temp, new MOVE(new MEM(new BINOP(0, new REG(new Reg("$gp")), this.sup.getVarTable().getInfo(tempKeys[i]).getAccess().getTree())),new CONST(0)) );
	   
	   return (Exp) new ESEQ(temp, dest);
	   

      }

   }


   
   public void dumpIRT() {

       String s = className.getText();
       if(superClass != null){
         s += "  Extends: "+getSuper().getText();
       } 
        
       System.out.println("-------------------------------------");
       System.out.println("Class: " +s);
       System.out.println("-------------------------------------");

       Print.prExp(genIRT());

     //   REG dest = new REG(new Reg("$dest"));
     //   if (this.vars.getVarNames().size()==0){
     //    MOVE static_link = new MOVE(dest, ((InFrame)info).getTree());
     //    ESEQ eseq = new ESEQ(static_link,dest);
     //    Print.prExp(eseq);
     //   }
     //   else {
     //    String[] tempKeys;
     //    if(superClass != null){
     //      int numVars = this.vars.getVarNames().size() + this.sup.getVarTable().getVarNames().size();
     //      tempKeys = new String[numVars];
     //      Set<String> superVars = this.sup.getVarTable().getVarNames();
     //      Set<String> sub = this.vars.getVarNames();
     //      int i = 0;
     //      for(String a : sub){
     //        tempKeys[i] = a;
     //        i++;
     //      }
     //      for(String a : superVars){
     //        tempKeys[i] = a;
     //        i++;
     //      }
     //    }
     //    else{
     //      int numVars = this.vars.getVarNames().size();
     //      tempKeys = new String[numVars];
     //      this.vars.getVarNames().toArray(tempKeys);
     //    }
     //    ExpList el = new ExpList(new CONST(tempKeys.length*4), null); 
       
     //    MOVE malloc = new MOVE(dest, new CALL(new NAME( new Label("malloc")),el  ));
     //    Stm temp;
     //    if (this.vars.getInfo(tempKeys[0]) != null)
     //      temp = new SEQ(malloc, new MOVE(new MEM(new BINOP(0, new REG(new Reg("base")), this.vars.getInfo(tempKeys[0]).getAccess().getTree())),new CONST(0)) );
     //    else
     //      temp = new SEQ(malloc, new MOVE(new MEM(new BINOP(0, new REG(new Reg("base")), this.sup.getVarTable().getInfo(tempKeys[0]).getAccess().getTree())),new CONST(0)) );
 
     // for(int i=1;i<tempKeys.length;i++)
     //    if (this.vars.getInfo(tempKeys[i]) != null)
     //      temp = new SEQ(temp, new MOVE(new MEM(new BINOP(0, new REG(new Reg("base")), this.vars.getInfo(tempKeys[i]).getAccess().getTree())),new CONST(0)) );          
     //    else
     //     temp = new SEQ(temp, new MOVE(new MEM(new BINOP(0, new REG(new Reg("base")), this.sup.getVarTable().getInfo(tempKeys[i]).getAccess().getTree())),new CONST(0)) );
     
     // Print.prExp(new ESEQ(temp, new REG(new Reg("$dest"))));

      // }
       //new BINOP(0, new REG(new Reg("base")), table.get(s).getAccess().getTree()))
       // Print.prExp(((InFrame)info).getTree(new REG(new Reg("dest"))));


       System.out.println("-------------------------------------");
       if (this.vars.getVarNames().size()>0) System.out.println("Instance var accessors:");
       vars.dumpIRT();
       methods.dumpIRT();
       

   } 
}
